﻿using LeadManagementEntities.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeadManagementDAO.ILeadMgmt
{
    public interface ILeadMgmtDAO
    {
        List<LM_Master_SourceEntities> GetLeadSource();
        bool InsertLeadSource(LM_Master_SourceInputEntities leadMgmtSourceInputEntities);

        List<LM_Master_CategoryEntities> GetLeadCategoryDetails();
        bool InsertLeadCategory(LM_Master_CategoryInputEntities leadMgmtCategoryInputEntities);

        List<LM_Master_StageEntities> GetLeadStageDetails();
        bool InsertLeadStage(LM_Master_StageInputEntities leadMgmtStageInputEntities);

        List<LM_Master_PriorityEntities> GetLeadPriorityDetails();
        bool InsertLeadPriority(LM_Master_PriorityInputEntities leadMgmtPriorityInputEntities);

        List<LM_LeadActions> ModifyLeadTicket(LM_Transaction_TicketModifyInputEntities ticketEntities);
        List<LM_LeadActions> GetLeadTimeLines(long leadId);
        List<long> GetLeadStakeHolders(long leadId);

        List<LM_Master_ActionEntities> GetLeadActionDetails();
        bool InsertLeadAction(LM_Master_ActionInputEntities leadMgmtActionInputEntities);

        List<LM_Master_CategoryStageMappingEntities> GetLeadCategoryStageMappingDetails();
        List<LM_Master_CategoryStageMappingEntities> GetLeadCategoryStageMapping(long categoryId);
        bool InsertLeadCategoryStageMapping(LM_Master_CategoryStageMappingInputEntities leadMgmtCategoryStageMappingInputEntities);

        List<LM_Master_ActionStageMappingEntities> GetLeadActionStageMappingDetails();
        bool InsertLeadActionStageMapping(LM_Master_ActionStageMappingInputEntities leadMgmtActionStageMappingInputEntities);

        List<LM_Transaction_AllLeadTicket> GetAllLeadTickets();

        List<LM_Transaction_AllLeadTicket> GetAllTicketsbyUserID(long UserId);
        List<LM_Transaction_AllLeadTicket> GetAllLead();
        
        List<LM_Transaction_AllLeadTicket> GetAllLeadsByUserId(long UserId);
        List<LM_Transaction_AllLeadTicket> GetAllTicketsByLeadId(long LeadId);
        bool InsertLeadTicket(LM_Transaction_LeadInputEntities leadMgmtActionStageMappingInputEntities);

        LM_Transaction_LiveTicket GetLiveTicket(long ticketId);
        List<LM_Transaction_TicketComments> GetLeadComments(long leadId);
        List<LM_Transaction_CrossSelling> GetCrossSellingLeads();
        LM_Transaction_CreateCrossSellingLead GetCrossSellingCustInfo(string cif_Id);
        List<LM_Transaction_ATM> GetATMLeads();
        LM_Transaction_CreateATMLead GetATMLeadCustInfo(long ATMLeadId);
    }
}
