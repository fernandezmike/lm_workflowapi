﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks; 
using LeadManagementEntities.Entities;
using System.Configuration;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
using System.Data;
using LeadManagementDAO.ILeadMgmt;

namespace LeadManagementDAO
{
    public class LeadMgmtDAO: ILeadMgmtDAO
    {
        #region Private Variables
        private static readonly string _sqlConnectionString = Convert.ToString(ConfigurationManager.AppSettings["SqlConnString"]);
        #endregion

        #region Public Methods
        public List<LM_Master_SourceEntities> GetLeadSource()
        {
            List<LM_Master_SourceEntities> sourceDetails = new List<LM_Master_SourceEntities>();
            SqlDataReader dr = null;
            try
            {
                dr = SqlHelper.ExecuteReader(_sqlConnectionString, CommandType.StoredProcedure, "usp_LG_GetSourceDetails");
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        LM_Master_SourceEntities entities = new LM_Master_SourceEntities();
                        entities.LeadSourceId = Convert.ToInt32(dr["LeadSourceId"]);
                        entities.Sourcename = Convert.ToString(dr["SourceName"]);
                        sourceDetails.Add(entities);
                    }
                }
                dr.Close();
            }
            finally
            {
                if (!dr.IsClosed && dr != null)
                {
                    dr.Close();
                }
            }
            return sourceDetails;
        }

        public bool InsertLeadSource(LM_Master_SourceInputEntities leadMgmtSourceInputEntities)
        {
            bool _isSuccess = false;

            SqlConnection conn = null;
            try
            {
                using (conn = new SqlConnection(_sqlConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("usp_LG_InsertSourceDetails", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.Parameters.Add("@SourceName", SqlDbType.VarChar).Value = leadMgmtSourceInputEntities.Sourcename;
                        cmd.Parameters.Add("@CreatedBy", SqlDbType.BigInt).Value = leadMgmtSourceInputEntities.UserId;

                        conn.Open();
                        int insertId = cmd.ExecuteNonQuery();
                        if(insertId != 0)
                        {
                            _isSuccess = true;
                        }
                    }
                }
            }
            finally
            {
                if (conn != null && conn.State == ConnectionState.Open)
                {
                    conn.Close();
                }
            }

            return _isSuccess;
        }

        //LM Category 

        public List<LM_Master_CategoryEntities> GetLeadCategoryDetails()
        {
            List<LM_Master_CategoryEntities> categoryDetails = new List<LM_Master_CategoryEntities>();
            SqlDataReader dr = null;
            try
            {
                dr = SqlHelper.ExecuteReader(_sqlConnectionString, CommandType.StoredProcedure, "usp_LM_GetLeadCategoryDetails");
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        LM_Master_CategoryEntities entities = new LM_Master_CategoryEntities();
                        entities.LeadCategoryId = Convert.ToInt32(dr["LeadCategoryId"]);
                        entities.LeadcategoryName = Convert.ToString(dr["LeadcategoryName"]);
                        categoryDetails.Add(entities);
                    }
                }
                dr.Close();
            }
            finally
            {
                if (!dr.IsClosed && dr != null)
                {
                    dr.Close();
                }
            }
            return categoryDetails;
        }

        public bool InsertLeadCategory(LM_Master_CategoryInputEntities leadMgmtCategoryInputEntities)
        {
            bool _isSuccess = false;

            SqlConnection conn = null;
            try
            {
                using (conn = new SqlConnection(_sqlConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("usp_LM_InsertLeadCategory", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.Parameters.Add("@LeadCategoryName", SqlDbType.VarChar).Value = leadMgmtCategoryInputEntities.LeadcategoryName;
                        cmd.Parameters.Add("@Description", SqlDbType.VarChar).Value = leadMgmtCategoryInputEntities.Description;
                        cmd.Parameters.Add("@CreatedBy", SqlDbType.BigInt).Value = leadMgmtCategoryInputEntities.CreatedBy;

                        conn.Open();
                        int insertId = cmd.ExecuteNonQuery();
                        if (insertId != 0)
                        {
                            _isSuccess = true;
                        }
                    }
                }
            }
            finally
            {
                if (conn != null && conn.State == ConnectionState.Open)
                {
                    conn.Close();
                }
            }

            return _isSuccess;
        }

        public List<LM_Master_StageEntities> GetLeadStageDetails()
        {
            List<LM_Master_StageEntities> stageDetails = new List<LM_Master_StageEntities>();
            SqlDataReader dr = null;
            try
            {
                dr = SqlHelper.ExecuteReader(_sqlConnectionString, CommandType.StoredProcedure, "usp_LM_GetLeadStageDetails");
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        LM_Master_StageEntities entities = new LM_Master_StageEntities();
                        entities.LeadStageId = Convert.ToInt32(dr["LeadStageId"]);
                        entities.StageName = Convert.ToString(dr["StageName"]);
                        entities.Description = Convert.ToString(dr["Description"]);
                        stageDetails.Add(entities);
                    }
                }
                dr.Close();
            }
            finally
            {
                if (!dr.IsClosed && dr != null)
                {
                    dr.Close();
                }
            }
            return stageDetails;
        }

        public bool InsertLeadStage(LM_Master_StageInputEntities leadMgmtStageInputEntities)
        {
            bool _isSuccess = false;

            SqlConnection conn = null;
            try
            {
                using (conn = new SqlConnection(_sqlConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("usp_LM_InsertLeadStage", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.Parameters.Add("@StageName", SqlDbType.VarChar).Value = leadMgmtStageInputEntities.StageName;
                        cmd.Parameters.Add("@Description", SqlDbType.VarChar).Value = leadMgmtStageInputEntities.Description;
                        cmd.Parameters.Add("@CreatedBy", SqlDbType.BigInt).Value = leadMgmtStageInputEntities.CreatedBy;

                        conn.Open();
                        int insertId = cmd.ExecuteNonQuery();
                        if (insertId != 0)
                        {
                            _isSuccess = true;
                        }
                    }
                }
            }
            finally
            {
                if (conn != null && conn.State == ConnectionState.Open)
                {
                    conn.Close();
                }
            }

            return _isSuccess;
        }

        public List<LM_Master_PriorityEntities> GetLeadPriorityDetails()
        {

            List<LM_Master_PriorityEntities> priorityDetails = new List<LM_Master_PriorityEntities>();
            SqlDataReader dr = null;
            try
            {
                dr = SqlHelper.ExecuteReader(_sqlConnectionString, CommandType.StoredProcedure, "usp_LM_GetLeadPriorityDetails");
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        LM_Master_PriorityEntities entities = new LM_Master_PriorityEntities();
                        entities.LeadPriorityId = Convert.ToInt32(dr["LeadPriorityId"]);
                        entities.StagePriority = Convert.ToString(dr["StagePriority"]);
                        entities.Description = Convert.ToString(dr["Description"]);
                        priorityDetails.Add(entities);
                    }
                }
                dr.Close();
            }
            finally
            {
                if (!dr.IsClosed && dr != null)
                {
                    dr.Close();
                }
            }
            return priorityDetails;
        }

        public bool InsertLeadPriority(LM_Master_PriorityInputEntities leadMgmtPriorityInputEntities)
        {
            bool _isSuccess = false;

            SqlConnection conn = null;
            try
            {
                using (conn = new SqlConnection(_sqlConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("usp_LM_InsertLeadPriority", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.Parameters.Add("@StagePriority", SqlDbType.VarChar).Value = leadMgmtPriorityInputEntities.StagePriority;
                        cmd.Parameters.Add("@Description", SqlDbType.VarChar).Value = leadMgmtPriorityInputEntities.Description;
                        cmd.Parameters.Add("@CreatedBy", SqlDbType.BigInt).Value = leadMgmtPriorityInputEntities.CreatedBy;

                        conn.Open();
                        int insertId = cmd.ExecuteNonQuery();
                        if (insertId != 0)
                        {
                            _isSuccess = true;
                        }
                    }
                }
            }
            finally
            {
                if (conn != null && conn.State == ConnectionState.Open)
                {
                    conn.Close();
                }
            }

            return _isSuccess;
        }

        public List<LM_Master_ActionEntities> GetLeadActionDetails()
        {
            List<LM_Master_ActionEntities> actionDetails = new List<LM_Master_ActionEntities>();
            SqlDataReader dr = null;
            try
            {
                dr = SqlHelper.ExecuteReader(_sqlConnectionString, CommandType.StoredProcedure, "usp_LM_GetLeadActionDetails");
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        LM_Master_ActionEntities entities = new LM_Master_ActionEntities();
                        entities.LeadActionId = Convert.ToInt32(dr["LeadActionId"]);
                        entities.Description = Convert.ToString(dr["Description"]);
                        actionDetails.Add(entities);
                    }
                }
                dr.Close();
            }
            finally
            {
                if (!dr.IsClosed && dr != null)
                {
                    dr.Close();
                }
            }
            return actionDetails;
        }

        public bool InsertLeadAction(LM_Master_ActionInputEntities leadMgmtActionInputEntities)
        {
            bool _isSuccess = false;

            SqlConnection conn = null;
            try
            {
                using (conn = new SqlConnection(_sqlConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("usp_LM_InsertLeadAction", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.Parameters.Add("@Description", SqlDbType.VarChar).Value = leadMgmtActionInputEntities.Description;
                        cmd.Parameters.Add("@CreatedBy", SqlDbType.BigInt).Value = leadMgmtActionInputEntities.CreatedBy;

                        conn.Open();
                        int insertId = cmd.ExecuteNonQuery();
                        if (insertId != 0)
                        {
                            _isSuccess = true;
                        }
                    }
                }
            }
            finally
            {
                if (conn != null && conn.State == ConnectionState.Open)
                {
                    conn.Close();
                }
            }

            return _isSuccess;
        }

        public List<LM_Master_CategoryStageMappingEntities> GetLeadCategoryStageMappingDetails()
        {
            List<LM_Master_CategoryStageMappingEntities> categoryStageMappingDetails = new List<LM_Master_CategoryStageMappingEntities>();
            SqlDataReader dr = null;
            try
            {
                dr = SqlHelper.ExecuteReader(_sqlConnectionString, CommandType.StoredProcedure, "usp_LM_GetLeadCategoryStageMappingDetails");
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        LM_Master_CategoryStageMappingEntities entities = new LM_Master_CategoryStageMappingEntities();
                        entities.LeadCategoryId = Convert.ToInt32(dr["LeadCategoryId"]);
                        entities.LeadStageId = Convert.ToInt32(dr["LeadStageId"]);
                        entities.SequenceId = Convert.ToInt32(dr["SequenceId"]);
                        entities.LeadMgmtStageEntities = new LM_Master_StageEntities();
                        entities.LeadMgmtStageEntities.StageName= Convert.ToString(dr["StageName"]);
                        //entities.LeadMgmtStageEntities.Description= Convert.ToString(dr["StageDescription"]);

                        entities.LeadMgmtCategoryEntities = new LM_Master_CategoryEntities();
                        entities.LeadMgmtCategoryEntities.LeadcategoryName = Convert.ToString(dr["LeadcategoryName"]);
                        //entities.LeadMgmtCategoryEntities.Description = Convert.ToString(dr["CategoryDescription"]);
                        categoryStageMappingDetails.Add(entities);
                    }
                }
                dr.Close();
            }
            finally
            {
                if (!dr.IsClosed && dr != null)
                {
                    dr.Close();
                }
            }
            return categoryStageMappingDetails;
        }

        public List<LM_Master_CategoryStageMappingEntities> GetLeadCategoryStageMapping(long categoryId)
        {
            List<LM_Master_CategoryStageMappingEntities> categoryStageMappingDetails = new List<LM_Master_CategoryStageMappingEntities>();
            SqlDataReader dr = null;
            try
            {
                dr = SqlHelper.ExecuteReader(_sqlConnectionString, CommandType.StoredProcedure, "usp_LM_GetLeadCategoryStageMappingByCategoryId", new SqlParameter("@CategoryId", categoryId));
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        LM_Master_CategoryStageMappingEntities entities = new LM_Master_CategoryStageMappingEntities();
                        entities.LeadCategoryId = Convert.ToInt32(dr["LeadCategoryId"]);
                        entities.LeadStageId = Convert.ToInt32(dr["LeadStageId"]);
                        entities.SequenceId = Convert.ToInt32(dr["SequenceId"]);
                        entities.LeadMgmtStageEntities = new LM_Master_StageEntities();
                        entities.LeadMgmtStageEntities.StageName = Convert.ToString(dr["StageName"]);
                        //entities.LeadMgmtStageEntities.Description = Convert.ToString(dr["StageDescription"]);

                        entities.LeadMgmtCategoryEntities = new LM_Master_CategoryEntities();
                        entities.LeadMgmtCategoryEntities.LeadcategoryName = Convert.ToString(dr["LeadcategoryName"]);
                        //entities.LeadMgmtCategoryEntities.Description = Convert.ToString(dr["CategoryDescription"]);
                        categoryStageMappingDetails.Add(entities);
                    }
                }
                dr.Close();
            }
            finally
            {
                if (!dr.IsClosed && dr != null)
                {
                    dr.Close();
                }
            }
            return categoryStageMappingDetails;
        }

        public bool InsertLeadCategoryStageMapping(LM_Master_CategoryStageMappingInputEntities leadMgmtCategoryStageMappingInputEntities)
        {
            bool _isSuccess = false;

            SqlConnection conn = null;
            try
            {
                using (conn = new SqlConnection(_sqlConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("usp_LM_InsertLeadCategoryStageMapping", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.Parameters.Add("@LeadCategoryId", SqlDbType.VarChar).Value = leadMgmtCategoryStageMappingInputEntities.LeadCategoryId;
                        cmd.Parameters.Add("@LeadStageId", SqlDbType.VarChar).Value = leadMgmtCategoryStageMappingInputEntities.LeadStageId;
                        cmd.Parameters.Add("@SequenceId", SqlDbType.VarChar).Value = leadMgmtCategoryStageMappingInputEntities.SequenceId;
                        cmd.Parameters.Add("@CreatedBy", SqlDbType.BigInt).Value = leadMgmtCategoryStageMappingInputEntities.CreatedBy;

                        conn.Open();
                        int insertId = cmd.ExecuteNonQuery();
                        if (insertId != 0)
                        {
                            _isSuccess = true;
                        }
                    }
                }
            }
            finally
            {
                if (conn != null && conn.State == ConnectionState.Open)
                {
                    conn.Close();
                }
            }

            return _isSuccess;
        }

        public List<LM_Master_ActionStageMappingEntities> GetLeadActionStageMappingDetails()
        {
            List<LM_Master_ActionStageMappingEntities> actionStageMappingDetails = new List<LM_Master_ActionStageMappingEntities>();
            SqlDataReader dr = null;
            try
            {
                dr = SqlHelper.ExecuteReader(_sqlConnectionString, CommandType.StoredProcedure, "usp_LM_GetLeadActionStageMappingDetails");
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        LM_Master_ActionStageMappingEntities entities = new LM_Master_ActionStageMappingEntities();
                        entities.LeadActionId = Convert.ToInt32(dr["LeadActionId"]);
                        entities.LeadStageId = Convert.ToInt32(dr["LeadStageId"]);
                        entities.SequenceId = Convert.ToInt32(dr["SequenceId"]);
                        entities.LeadMgmtStageEntities = new LM_Master_StageEntities();
                        entities.LeadMgmtStageEntities.StageName = Convert.ToString(dr["StageName"]);
                        entities.LeadMgmtStageEntities.Description = Convert.ToString(dr["StageDescription"]);

                        entities.LeadMgmtActionEntities = new LM_Master_ActionEntities();
                        entities.LeadMgmtActionEntities.Description = Convert.ToString(dr["ActionDescription"]);
                        actionStageMappingDetails.Add(entities);
                    }
                }
                dr.Close();
            }
            finally
            {
                if (!dr.IsClosed && dr != null)
                {
                    dr.Close();
                }
            }
            return actionStageMappingDetails;
        }

        public bool InsertLeadActionStageMapping(LM_Master_ActionStageMappingInputEntities leadMgmtActionStageMappingInputEntities)
        {
            bool _isSuccess = false;

            SqlConnection conn = null;
            try
            {
                using (conn = new SqlConnection(_sqlConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("usp_LM_InsertLeadActionStageMapping", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.Parameters.Add("@LeadActionId", SqlDbType.VarChar).Value = leadMgmtActionStageMappingInputEntities.LeadActionId;
                        cmd.Parameters.Add("@LeadStageId", SqlDbType.VarChar).Value = leadMgmtActionStageMappingInputEntities.LeadStageId;
                        cmd.Parameters.Add("@SequenceId", SqlDbType.VarChar).Value = leadMgmtActionStageMappingInputEntities.SequenceId;
                        cmd.Parameters.Add("@CreatedBy", SqlDbType.BigInt).Value = leadMgmtActionStageMappingInputEntities.CreatedBy;

                        conn.Open();
                        int insertId = cmd.ExecuteNonQuery();
                        if (insertId != 0)
                        {
                            _isSuccess = true;
                        }
                    }
                }
            }
            finally
            {
                if (conn != null && conn.State == ConnectionState.Open)
                {
                    conn.Close();
                }
            }

            return _isSuccess;
        }

        public List<LM_Transaction_AllLeadTicket> GetAllLeadTickets()
        {
            List<LM_Transaction_AllLeadTicket> sourceDetails = new List<LM_Transaction_AllLeadTicket>();
            SqlDataReader dr = null;
            try
            {
                dr = SqlHelper.ExecuteReader(_sqlConnectionString, CommandType.StoredProcedure, "usp_LM_GetAllLeadTicketDetails");
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        LM_Transaction_AllLeadTicket entities = new LM_Transaction_AllLeadTicket();
                        entities.TicketId = Convert.ToInt32(dr["TicketId"]);
                        entities.LeadDescription = Convert.ToString(dr["LeadDescription"]);
                        entities.LeadId = Convert.ToInt32(dr["LeadId"]);
                        entities.ProductId = Convert.ToInt32(dr["ProductId"]);
                        entities.ProductName = Convert.ToString(dr["ProductName"]);
                        entities.LeadStageId = Convert.ToInt32(dr["LeadStageId"]);
                        entities.CreatedBy = Convert.ToInt32(dr["CreatedBy"]);
                        entities.TicketAssigineeId = Convert.ToInt32(dr["TicketAssigineeId"]);
                        entities.TicketAssiginee = Convert.ToString(dr["TicketAssiginee"]);
                        entities.Status = Convert.ToString(dr["StageName"]);
                        entities.CreatedOn = Convert.ToDateTime(dr["CreatedOn"]);
                        entities.UpdatedOn = Convert.ToDateTime(dr["UpdatedOn"]);
                        entities.Sourcename = Convert.ToString(dr["Sourcename"]);
                        entities.LeadCategoryName = Convert.ToString(dr["LeadcategoryName"]);
                        sourceDetails.Add(entities);

                    }
                }
                dr.Close();
            }
            finally
            {
                if (!dr.IsClosed && dr != null)
                {
                    dr.Close();
                }
            }
            return sourceDetails;
        }

        public List<LM_Transaction_AllLeadTicket> GetAllTicketsbyUserID(long UserId)
        {
            List<LM_Transaction_AllLeadTicket> sourceDetails = new List<LM_Transaction_AllLeadTicket>();
            SqlDataReader dr = null;
            try
            {
                dr = SqlHelper.ExecuteReader(_sqlConnectionString, CommandType.StoredProcedure, "usp_LM_GetAllLeadTicketDetailsByUserId", new SqlParameter("@UserId", UserId));
                
                    if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        LM_Transaction_AllLeadTicket entities = new LM_Transaction_AllLeadTicket();
                        entities.TicketId = Convert.ToInt32(dr["TicketId"]);
                        entities.LeadDescription = Convert.ToString(dr["LeadDescription"]);
                        entities.LeadId = Convert.ToInt32(dr["LeadId"]);
                        entities.ProductId = Convert.ToInt32(dr["ProductId"]);
                        entities.ProductName = Convert.ToString(dr["ProductName"]);
                        entities.LeadStageId = Convert.ToInt32(dr["LeadStageId"]);
                        entities.CreatedBy = Convert.ToInt32(dr["CreatedBy"]);
                        entities.TicketAssigineeId = Convert.ToInt32(dr["TicketAssigineeId"]);
                        entities.TicketAssiginee = Convert.ToString(dr["TicketAssiginee"]);
                        entities.Status = Convert.ToString(dr["StageName"]);
                        entities.CreatedOn = Convert.ToDateTime(dr["CreatedOn"]);
                        entities.UpdatedOn = Convert.ToDateTime(dr["UpdatedOn"]);
                        entities.Sourcename = Convert.ToString(dr["Sourcename"]);
                        entities.LeadCategoryName = Convert.ToString(dr["LeadcategoryName"]);
                        sourceDetails.Add(entities);

                    }
                }
                dr.Close();
            }
            finally
            {
                if (!dr.IsClosed && dr != null)
                {
                    dr.Close();
                }
            }
            return sourceDetails;
        }
        public List<LM_Transaction_AllLeadTicket> GetAllLead()
        {
            List<LM_Transaction_AllLeadTicket> sourceDetails = new List<LM_Transaction_AllLeadTicket>();
            SqlDataReader dr = null;
            try
            {
                dr = SqlHelper.ExecuteReader(_sqlConnectionString, CommandType.StoredProcedure, "usp_LM_GetAllLead");
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        LM_Transaction_AllLeadTicket entities = new LM_Transaction_AllLeadTicket();
                        entities.LeadId = Convert.ToInt32(dr["LeadId"]);
                        //entities.TicketId = Convert.ToInt32(dr["TicketId"]);
                        entities.LeadDescription = Convert.ToString(dr["LeadDescription"]);
                        entities.Sourcename = Convert.ToString(dr["Sourcename"]);
                        entities.Score = Convert.ToInt32(dr["Score"]);
                        entities.ProductName = Convert.ToString(dr["ProductName"]);
                        entities.IsActive = Convert.ToBoolean(dr["IsActive"]);
                        entities.CreatedOn = Convert.ToDateTime(dr["CreatedOn"]);
                        entities.UpdatedOn = Convert.ToDateTime(dr["UpdatedOn"]);
                        sourceDetails.Add(entities);

                    }
                }
                dr.Close();
            }
            finally
            {
                if (!dr.IsClosed && dr != null)
                {
                    dr.Close();
                }
            }
            return sourceDetails;
        }
        public List<LM_Transaction_AllLeadTicket> GetAllTicketsByLeadId(long LeadId)
        {
            List<LM_Transaction_AllLeadTicket> sourceDetails = new List<LM_Transaction_AllLeadTicket>();
            SqlDataReader dr = null;
            try
            {
                dr = SqlHelper.ExecuteReader(_sqlConnectionString, CommandType.StoredProcedure, "usp_LM_GetAllTicketsByLeadId", new SqlParameter("@LeadId", LeadId));
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        LM_Transaction_AllLeadTicket entities = new LM_Transaction_AllLeadTicket();
                        entities.TicketId = Convert.ToInt32(dr["TicketId"]);
                        entities.LeadDescription = Convert.ToString(dr["LeadDescription"]);
                        entities.LeadId = Convert.ToInt32(dr["LeadId"]);
                        entities.ProductName = Convert.ToString(dr["ProductName"]);
                        entities.LeadStageId = Convert.ToInt32(dr["LeadStageId"]);
                        entities.CreatedBy = Convert.ToInt32(dr["CreatedBy"]);
                        entities.TicketAssiginee = Convert.ToString(dr["TicketAssigineeId"]);
                        entities.Status = Convert.ToString(dr["StageName"]);
                        entities.CreatedOn = Convert.ToDateTime(dr["CreatedOn"]);
                        entities.UpdatedOn = Convert.ToDateTime(dr["UpdatedOn"]);
                        entities.Sourcename = Convert.ToString(dr["Sourcename"]);
                        entities.LeadCategoryName = Convert.ToString(dr["LeadcategoryName"]);
                        sourceDetails.Add(entities);
                    }
                }
               
                dr.Close();
            }
            finally
            {
                if (!dr.IsClosed && dr != null)
                {
                    dr.Close();
                }
            }
            return sourceDetails;
        }
        public List<LM_Transaction_AllLeadTicket> GetAllLeadsByUserId(long UserId)
        {
            List<LM_Transaction_AllLeadTicket> sourceDetails = new List<LM_Transaction_AllLeadTicket>();
            SqlDataReader dr = null;
            try
            {
                dr = SqlHelper.ExecuteReader(_sqlConnectionString, CommandType.StoredProcedure, "usp_LM_GetAllLeadTicketDetailsByUserId", new SqlParameter("@UserId", UserId));
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        LM_Transaction_AllLeadTicket entities = new LM_Transaction_AllLeadTicket();
                        entities.TicketId = Convert.ToInt32(dr["TicketId"]);
                        entities.LeadDescription = Convert.ToString(dr["LeadDescription"]);
                        entities.LeadId = Convert.ToInt32(dr["LeadId"]);
                        entities.ProductId = Convert.ToInt32(dr["ProductId"]);
                        entities.LeadStageId = Convert.ToInt32(dr["LeadStageId"]);
                        entities.CreatedBy = Convert.ToInt32(dr["CreatedBy"]);
                        entities.TicketAssigineeId = Convert.ToInt32(dr["TicketAssigineeId"]);
                        entities.Status = Convert.ToString(dr["StageName"]);
                        entities.CreatedOn = Convert.ToDateTime(dr["CreatedOn"]);
                        entities.UpdatedOn = Convert.ToDateTime(dr["UpdatedOn"]);
                        entities.Sourcename = Convert.ToString(dr["Sourcename"]);
                        entities.LeadCategoryName = Convert.ToString(dr["LeadcategoryName"]);
                        sourceDetails.Add(entities);
                    }
                }
                dr.Close();
            }
            finally
            {
                if (!dr.IsClosed && dr != null)
                {
                    dr.Close();
                }
            }
            return sourceDetails;
        }


        public bool InsertLeadTicket(LM_Transaction_LeadInputEntities leadMgmtTicketInputEntities)
        {
            bool _isSuccess = false;

            SqlConnection conn = null;
            try
            {
                using (conn = new SqlConnection(_sqlConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("usp_LM_InsertLeadDetails", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.Parameters.Add("@LeadDescription", SqlDbType.VarChar).Value = leadMgmtTicketInputEntities.LeadDescription;
                        cmd.Parameters.Add("@CustomerName", SqlDbType.VarChar).Value = leadMgmtTicketInputEntities.CustomerName;
                        cmd.Parameters.Add("@CustomerEmail", SqlDbType.VarChar).Value = leadMgmtTicketInputEntities.CustomerEmail;
                        cmd.Parameters.Add("@IsExistingCustomer", SqlDbType.VarChar).Value = leadMgmtTicketInputEntities.IsExistingCustomer;
                        cmd.Parameters.Add("@CIFID", SqlDbType.VarChar).Value = leadMgmtTicketInputEntities.CIFID;
                        cmd.Parameters.Add("@ProductId", SqlDbType.VarChar).Value = leadMgmtTicketInputEntities.ProductId;
                        cmd.Parameters.Add("@SourceId", SqlDbType.VarChar).Value = leadMgmtTicketInputEntities.LeadSourceId;
                        cmd.Parameters.Add("@CustomerContactNo", SqlDbType.VarChar).Value = leadMgmtTicketInputEntities.CustomerContactNo;
                        cmd.Parameters.Add("@Path", SqlDbType.VarChar).Value = leadMgmtTicketInputEntities.AttachmentPath;
                        cmd.Parameters.Add("@LeadCategoryId", SqlDbType.VarChar).Value = leadMgmtTicketInputEntities.CategoryId;
                        cmd.Parameters.Add("@RegionId", SqlDbType.VarChar).Value = leadMgmtTicketInputEntities.RegionId;
                        cmd.Parameters.Add("@TicketAssigineeId", SqlDbType.VarChar).Value = leadMgmtTicketInputEntities.TicketAssigineeId;
                        cmd.Parameters.Add("@Comments", SqlDbType.VarChar).Value = leadMgmtTicketInputEntities.Comments;
                        cmd.Parameters.Add("@CreatedBy", SqlDbType.BigInt).Value = leadMgmtTicketInputEntities.CreatedBy;
                        cmd.Parameters.Add("@IsCrossLead", SqlDbType.Bit).Value = leadMgmtTicketInputEntities.IsCrossLead;
                        cmd.Parameters.Add("@ATMLeadId", SqlDbType.BigInt).Value = leadMgmtTicketInputEntities.ATMLeadId;

                        conn.Open();
                        int insertId = cmd.ExecuteNonQuery();
                        if (insertId != 0)
                        {
                            _isSuccess = true;
                        }
                    }
                }
            }
            finally
            {
                if (conn != null && conn.State == ConnectionState.Open)
                {
                    conn.Close();
                }
            }

            return _isSuccess;
        }
        public List<LM_LeadActions> ModifyLeadTicket(LM_Transaction_TicketModifyInputEntities leadMgmtTicketInputEntities)
        {
            List<LM_LeadActions> lstLM_LeadActions = new List<LM_LeadActions>();
            SqlDataReader dr = null;
            try
            {
                dr = SqlHelper.ExecuteReader(_sqlConnectionString, CommandType.StoredProcedure, "usp_LM_ModifyLeadTicket_test", new SqlParameter("@TicketId", leadMgmtTicketInputEntities.TicketId)
                , new SqlParameter("@LeadStageId", leadMgmtTicketInputEntities.LeadStageId)
                , new SqlParameter("@LeadId", leadMgmtTicketInputEntities.LeadId)
                , new SqlParameter("@Comments", leadMgmtTicketInputEntities.Comments)
                , new SqlParameter("@CreatedBy", leadMgmtTicketInputEntities.CreatedBy)
                , new SqlParameter("@LeadScore", leadMgmtTicketInputEntities.LeadScore)
                , new SqlParameter("@Revenue", leadMgmtTicketInputEntities.Revenue)
                , new SqlParameter("@ProductId", leadMgmtTicketInputEntities.ProductId)
                , new SqlParameter("@AssigineeId", leadMgmtTicketInputEntities.AssigineeId)
                , new SqlParameter("@LeadCategoryId", leadMgmtTicketInputEntities.LeadCategoryId)
                , new SqlParameter("@IsActive", leadMgmtTicketInputEntities.IsActive)
                , new SqlParameter("@BranchId", leadMgmtTicketInputEntities.BranchID));
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        LM_LeadActions entities = new LM_LeadActions();
                        entities.LeadId = Convert.ToInt32(dr["LeadId"]);
                        entities.TicketId = Convert.ToInt32(dr["TicketId"]);
                        entities.LeadAction = Convert.ToString(dr["LeadAction"]);
                        entities.CreatedOn = Convert.ToDateTime(dr["CreatedOn"]);
                        entities.CreatedBy = Convert.ToInt32(dr["CreatedBy"]);
                        lstLM_LeadActions.Add(entities);
                    }
                }
                dr.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (!dr.IsClosed && dr != null)
                {
                    dr.Close();
                }
            }
            return lstLM_LeadActions;
        }

        public List<LM_LeadActions> GetLeadTimeLines(long leadId)
        {
            List<LM_LeadActions> lstLM_LeadActions = new List<LM_LeadActions>();
            SqlDataReader dr = null;
            try
            {
                dr = SqlHelper.ExecuteReader(_sqlConnectionString, CommandType.StoredProcedure, "usp_LM_GetLeadTimeLines", new SqlParameter("@LeadId", leadId));
                {
                    while (dr.Read())
                    {
                        LM_LeadActions entities = new LM_LeadActions();
                        entities.LeadId = Convert.ToInt32(dr["LeadId"]);
                        entities.TicketId = DBNull.Value.Equals(dr["TicketId"]) ? 0 : Int32.Parse(dr["TicketId"].ToString());
                        entities.LeadAction = Convert.ToString(dr["LeadAction"]);
                        entities.CreatedOn = Convert.ToDateTime(dr["CreatedOn"]);
                        entities.CreatedBy = Convert.ToInt32(dr["CreatedBy"]);
                        lstLM_LeadActions.Add(entities);
                    }
                }
                dr.Close();
            }
            finally
            {
                if (!dr.IsClosed && dr != null)
                {
                    dr.Close();
                }
            }
            return lstLM_LeadActions;
        }

        public List<long> GetLeadStakeHolders(long leadId)
        {
            List<long> lstLM_StakeHolders = new List<long>();
            long StakeHolderId;
            SqlDataReader dr = null;
            try
            {
                dr = SqlHelper.ExecuteReader(_sqlConnectionString, CommandType.StoredProcedure, "usp_LM_GetLeadStakeHolder", new SqlParameter("@LeadId", leadId));
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        StakeHolderId = Convert.ToInt32(dr["StakeHolderId"]);
                        lstLM_StakeHolders.Add(StakeHolderId);
                    }
                }
                dr.Close();
            }
            finally
            {
                if (!dr.IsClosed && dr != null)
                {
                    dr.Close();
                }
            }
            return lstLM_StakeHolders;
        }

        public LM_Transaction_LiveTicket GetLiveTicket(long ticketId)
        {
            LM_Transaction_LiveTicket entities = new LM_Transaction_LiveTicket();
            SqlDataReader dr = null;
            try
            {
                dr = SqlHelper.ExecuteReader(_sqlConnectionString, CommandType.StoredProcedure, "usp_LM_GetLiveTicket",new SqlParameter("@TicketId",ticketId));
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        entities.TicketId = Convert.ToInt32(dr["TicketId"]);
                        entities.LeadId = Convert.ToInt32(dr["LeadId"]);
                        entities.CategoryId = Convert.ToInt32(dr["LeadCategoryId"]);
                        entities.LeadStageId = Convert.ToInt32(dr["LeadStageId"]);
                        entities.ProductId = Convert.ToInt32(dr["ProductId"]);
                        entities.LeadDescription = Convert.ToString(dr["LeadDescription"]);
                        entities.LeadCategoryName = Convert.ToString(dr["LeadcategoryName"]);
                        entities.TicketAssigineeId = Convert.ToInt32(dr["TicketAssigineeId"]);
                        entities.CreatedBy = Convert.ToInt32(dr["CreatedBy"]);
                        entities.CustomerName = Convert.ToString(dr["CustomerName"]);
                        entities.CustomerContactNo = Convert.ToString(dr["CustomerContactNo"]);
                        entities.CustomerEmail = Convert.ToString(dr["CustomerEmail"]);
                        entities.LeadStage = Convert.ToString(dr["StageName"]);
                        entities.LeadScore = Convert.ToInt32(dr["LeadScore"]);
                        entities.BranchId = Convert.ToInt32(dr["BranchId"]);
                        entities.LeadRevenue = Convert.ToInt32(dr["LeadRevenue"]);
                        entities.CreatedOn = Convert.ToDateTime(dr["CreatedOn"]);
                        entities.UpdatedOn = Convert.ToDateTime(dr["UpdatedOn"]);
                        entities.CommentCount = Convert.ToInt32(dr["CommentCount"]);
                        entities.Source = Convert.ToString(dr["Sourcename"]);
                        entities.IsActive = Convert.ToBoolean(dr["IsActive"]);
                    }
                }
                dr.Close();
            }
            finally
            {
                if (!dr.IsClosed && dr != null)
                {
                    dr.Close();
                }
            }
            return entities;
        }
        public List<LM_Transaction_TicketComments> GetLeadComments(long leadId)
        {
            List<LM_Transaction_TicketComments> commentDetails = new List<LM_Transaction_TicketComments>();
            SqlDataReader dr = null;
            try
            {
                dr = SqlHelper.ExecuteReader(_sqlConnectionString, CommandType.StoredProcedure, "usp_LM_GetLeadComments", new SqlParameter("@LeadId", leadId));
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        LM_Transaction_TicketComments entities = new LM_Transaction_TicketComments();
                        entities.CreatedBy = Convert.ToInt32(dr["CreatedBy"]);
                        entities.Comments = Convert.ToString(dr["Comments"]);
                        entities.CommentId = Convert.ToInt32(dr["CommentId"]);
                        entities.UserName = Convert.ToString(dr["UserName"]); 
                        entities.CreatedOn = Convert.ToDateTime(dr["CreatedOn"]);
                        commentDetails.Add(entities);
                    }
                }
                dr.Close();
            }
            finally
            {
                if (!dr.IsClosed && dr != null)
                {
                    dr.Close();
                }
            }
            return commentDetails;
        }

        public List<LM_Transaction_CrossSelling> GetCrossSellingLeads()
        {
            List<LM_Transaction_CrossSelling> crossSelling = new List<LM_Transaction_CrossSelling>();
            SqlDataReader dr = null;
            try
            {
                dr = SqlHelper.ExecuteReader(_sqlConnectionString, CommandType.StoredProcedure, "usp_LM_GetCrossSellingLeads");
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        LM_Transaction_CrossSelling entities = new LM_Transaction_CrossSelling();
                        entities.CIF_ID = Convert.ToString(dr["cif_id"]);
                        //entities.TicketId = Convert.ToInt32(dr["TicketId"]);
                        entities.Acct_no = Convert.ToDouble(dr["Acc_Num"]);
                        entities.Cust_Name = Convert.ToString(dr["full_name"]);
                        entities.Product = Convert.ToString(dr["Product"]);
                        crossSelling.Add(entities);

                    }
                }
                dr.Close();
            }
            finally
            {
                if (!dr.IsClosed && dr != null)
                {
                    dr.Close();
                }
            }
            return crossSelling;
        }

        public LM_Transaction_CreateCrossSellingLead GetCrossSellingCustInfo(string cif_Id)
        {
            LM_Transaction_CreateCrossSellingLead entities = new LM_Transaction_CreateCrossSellingLead();
            SqlDataReader dr = null;
            try
            {
                dr = SqlHelper.ExecuteReader(_sqlConnectionString, CommandType.StoredProcedure, "usp_LM_GetCrossSellingLeadsByCIFID", new SqlParameter("@CIF_ID", cif_Id));
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        entities.LeadTitle = Convert.ToString(dr["LeadTitle"]);
                        entities.Cust_Name = Convert.ToString(dr["full_name"]);
                        entities.Mobile_No = Convert.ToString(dr["mobile"]);
                        entities.CIF_ID = Convert.ToString(dr["cif_id"]);
                        entities.Lead_Source = Convert.ToString(dr["LeadSource"]);
                        entities.Product = Convert.ToString(dr["Product"]);
                    }
                }
                dr.Close();
            }
            finally
            {
                if (!dr.IsClosed && dr != null)
                {
                    dr.Close();
                }
            }
            return entities;
        }

        public List<LM_Transaction_ATM> GetATMLeads()
        {
            List<LM_Transaction_ATM> aTMs = new List<LM_Transaction_ATM>();
            SqlDataReader dr = null;
            try
            {
                dr = SqlHelper.ExecuteReader(_sqlConnectionString, CommandType.StoredProcedure, "usp_LM_GetATMLeads");
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        LM_Transaction_ATM entities = new LM_Transaction_ATM();
                        entities.ATMLeadId = Convert.ToInt64(dr["ATMLeadId"]);
                        entities.CardNo = Convert.ToString(dr["CardNo"]);
                        entities.Mobile = Convert.ToString(dr["Mobile"]);
                        entities.Product = Convert.ToString(dr["Product"]);
                        aTMs.Add(entities);

                    }
                }
                dr.Close();
            }
            finally
            {
                if (!dr.IsClosed && dr != null)
                {
                    dr.Close();
                }
            }
            return aTMs;
        }

        public LM_Transaction_CreateATMLead GetATMLeadCustInfo(long ATMLeadId)
        {
            LM_Transaction_CreateATMLead aTMLeadItem = new LM_Transaction_CreateATMLead();
            SqlDataReader dr = null;
            try
            {
                dr = SqlHelper.ExecuteReader(_sqlConnectionString, CommandType.StoredProcedure, "usp_LM_GetATMLeadById", new SqlParameter("@ATMLeadId", ATMLeadId));
                if (dr.HasRows)
                {
                    if (dr.Read())
                    {
                        aTMLeadItem.Mobile = Convert.ToString(dr["Mobile"]);
                        aTMLeadItem.Product = Convert.ToString(dr["Product"]);
                        aTMLeadItem.LeadSourceId = Convert.ToInt64(dr["LeadSourceId"]);
                        aTMLeadItem.LeadCategoryId = Convert.ToInt64(dr["LeadCategoryId"]);
                        aTMLeadItem.ProductId = Convert.ToInt64(dr["ProductId"]);
                    }
                }
                dr.Close();
            }
            finally
            {
                if (!dr.IsClosed && dr != null)
                {
                    dr.Close();
                }
            }
            return aTMLeadItem;
        }
        #endregion

        #region Private Methods

        #endregion
    }
}
