﻿using LeadManagementEntities.Entities;
using LeadManagementBO.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks; 
using LeadManagementEntities.Error;
using LeadManagementDAO.ILeadMgmt;

namespace LeadManagementBO
{
    public class LeadMgmtBO: ILeadMgmtBO
    {
        #region Private Variables
        private readonly ILeadMgmtDAO _leadmgmtDAO = null;

        #endregion

        public LeadMgmtBO(ILeadMgmtDAO leadmgmtDAO)
        {
            _leadmgmtDAO = leadmgmtDAO;
        }

        #region Public Methods
        public List<LM_Master_SourceEntities> GetLeadSource()
        {
            List<LM_Master_SourceEntities> _leadDetails = _leadmgmtDAO.GetLeadSource();
            return _leadDetails;
        }
        
        public bool InsertLeadSource(LM_Master_SourceInputEntities leadMgmtSourceInputEntities)
        {
            ValidateLeadMgmtSourceInput(leadMgmtSourceInputEntities);
            bool _isInsertSuccess = _leadmgmtDAO.InsertLeadSource(leadMgmtSourceInputEntities);
            return _isInsertSuccess;
        }


        public List<LM_Master_CategoryEntities> GetLeadCategoryDetails()
        {
            List<LM_Master_CategoryEntities> _leadCategoryDetails = _leadmgmtDAO.GetLeadCategoryDetails();
            return _leadCategoryDetails;
        }

        public bool InsertLeadCategory(LM_Master_CategoryInputEntities leadMgmtCategoryInputEntities)
        {
            ValidateLeadMgmtCategoryInput(leadMgmtCategoryInputEntities);
            bool _isInsertSuccess = _leadmgmtDAO.InsertLeadCategory(leadMgmtCategoryInputEntities);
            return _isInsertSuccess;
        }


        public List<LM_LeadActions> ModifyLeadTicket(LM_Transaction_TicketModifyInputEntities ticketEntities)
        {
            List<LM_LeadActions> _leadActions = _leadmgmtDAO.ModifyLeadTicket(ticketEntities);
            return _leadActions;
        }

        public List<LM_LeadActions> GetLeadTimeLines(long leadId)
        {
            List<LM_LeadActions> _leadActions = _leadmgmtDAO.GetLeadTimeLines(leadId);
            return _leadActions;
        }

        public List<long> GetLeadStakeHolders(long leadId)
        {
            List<long> _leadStakeHolders = _leadmgmtDAO.GetLeadStakeHolders(leadId);
            return _leadStakeHolders;
        }

        public List<LM_Master_StageEntities> GetLeadStageDetails()
        {
            List<LM_Master_StageEntities> _leadDetails = _leadmgmtDAO.GetLeadStageDetails();
            return _leadDetails;
        }

        public bool InsertLeadStage(LM_Master_StageInputEntities leadMgmtStageInputEntities)
        {
            ValidateLeadMgmtStageInput(leadMgmtStageInputEntities);
            bool _isInsertSuccess = _leadmgmtDAO.InsertLeadStage(leadMgmtStageInputEntities);
            return _isInsertSuccess;
        }

        public List<LM_Master_PriorityEntities> GetLeadPriorityDetails()
        {
            List<LM_Master_PriorityEntities> _leadDetails = _leadmgmtDAO.GetLeadPriorityDetails();
            return _leadDetails;
        }

        public bool InsertLeadPriority(LM_Master_PriorityInputEntities leadMgmtPriorityInputEntities)
        {
            ValidateLeadMgmtPriorityInput(leadMgmtPriorityInputEntities);
            bool _isInsertSuccess = _leadmgmtDAO.InsertLeadPriority(leadMgmtPriorityInputEntities);
            return _isInsertSuccess;
        }

        public List<LM_Master_ActionEntities> GetLeadActionDetails()
        {
            List<LM_Master_ActionEntities> _leadDetails = _leadmgmtDAO.GetLeadActionDetails();
            return _leadDetails;
        }

        public bool InsertLeadAction(LM_Master_ActionInputEntities leadMgmtActionInputEntities)
        {
            ValidateLeadMgmtActionInput(leadMgmtActionInputEntities);
            bool _isInsertSuccess = _leadmgmtDAO.InsertLeadAction(leadMgmtActionInputEntities);
            return _isInsertSuccess;
        }

        public List<LM_Master_CategoryStageMappingEntities> GetLeadCategoryStageMappingDetails()
        {
            List<LM_Master_CategoryStageMappingEntities> _leadDetails = _leadmgmtDAO.GetLeadCategoryStageMappingDetails();
            return _leadDetails;
        }

        public List<LM_Master_CategoryStageMappingEntities> GetLeadCategoryStageMapping(long categoryId)
        {
            List<LM_Master_CategoryStageMappingEntities> _leadDetails = _leadmgmtDAO.GetLeadCategoryStageMapping(categoryId);
            return _leadDetails;
        }
        public bool InsertLeadCategoryStageMapping(LM_Master_CategoryStageMappingInputEntities leadMgmtCategoryStageMappingInputEntities)
        {
            ValidateLeadMgmtCategoryStageMappingInput(leadMgmtCategoryStageMappingInputEntities);
            bool _isInsertSuccess = _leadmgmtDAO.InsertLeadCategoryStageMapping(leadMgmtCategoryStageMappingInputEntities);
            return _isInsertSuccess;
        }

        public List<LM_Master_ActionStageMappingEntities> GetLeadActionStageMappingDetails()
        {
            List<LM_Master_ActionStageMappingEntities> _leadDetails = _leadmgmtDAO.GetLeadActionStageMappingDetails();
            return _leadDetails;
        }

        public bool InsertLeadActionStageMapping(LM_Master_ActionStageMappingInputEntities leadMgmtActionStageMappingInputEntities)
        {
            ValidateLeadMgmtActionStageMappingInput(leadMgmtActionStageMappingInputEntities);
            bool _isInsertSuccess = _leadmgmtDAO.InsertLeadActionStageMapping(leadMgmtActionStageMappingInputEntities);
            return _isInsertSuccess;
        }

        public List<LM_Transaction_AllLeadTicket> GetAllLeadTickets()
        {
            List<LM_Transaction_AllLeadTicket> _leadDetails = _leadmgmtDAO.GetAllLeadTickets();
            return _leadDetails;
        }

        public List<LM_Transaction_AllLeadTicket> GetAllTicketsbyUserID(long UserId)
        {
            List<LM_Transaction_AllLeadTicket> _leadDetails = _leadmgmtDAO.GetAllTicketsbyUserID(UserId);
            return _leadDetails;
        }
        public List<LM_Transaction_AllLeadTicket> GetAllLead()
        {
            List<LM_Transaction_AllLeadTicket> _leadDetails = _leadmgmtDAO.GetAllLead();
            return _leadDetails;
        }
        

        public List<LM_Transaction_AllLeadTicket> GetAllLeadsByUserId(long UserId)
        {
            List<LM_Transaction_AllLeadTicket> _leadDetails = _leadmgmtDAO.GetAllLeadsByUserId(UserId);
            return _leadDetails;
        }
        public List<LM_Transaction_AllLeadTicket> GetAllTicketsByLeadId(long LeadId)
        {
            List<LM_Transaction_AllLeadTicket> _leadDetails = _leadmgmtDAO.GetAllTicketsByLeadId(LeadId);
            return _leadDetails;
        }
        public bool InsertLeadTicket(LM_Transaction_LeadInputEntities leadMgmtLeadTicketInputEntities)
        {
            bool _isInsertSuccess = _leadmgmtDAO.InsertLeadTicket(leadMgmtLeadTicketInputEntities);
            return _isInsertSuccess;
        }


        public LM_Transaction_LiveTicket GetLiveTicket(long ticketId)
        {
            LM_Transaction_LiveTicket _ticketDetails = _leadmgmtDAO.GetLiveTicket(ticketId);
            return _ticketDetails;
        }

        public List<LM_Transaction_TicketComments> GetLeadComments(long leadId)
        {
            List<LM_Transaction_TicketComments> _commentDetails = _leadmgmtDAO.GetLeadComments(leadId);
            return _commentDetails;
        }

        public List<LM_Transaction_CrossSelling> GetCrossSellingLeads()
        {
            List<LM_Transaction_CrossSelling> _leadDetails = _leadmgmtDAO.GetCrossSellingLeads();
            return _leadDetails;
        }

        public LM_Transaction_CreateCrossSellingLead GetCrossSellingCustInfo(string cif_Id)
        {
            LM_Transaction_CreateCrossSellingLead _ticketDetails = _leadmgmtDAO.GetCrossSellingCustInfo(cif_Id);
            return _ticketDetails;
        }

        public List<LM_Transaction_ATM> GetATMLeads()
        {
            List<LM_Transaction_ATM> _aTMLeadDetails = _leadmgmtDAO.GetATMLeads();
            return _aTMLeadDetails;
        }

        public LM_Transaction_CreateATMLead GetATMLeadCustInfo(long ATMLeadId)
        {
            LM_Transaction_CreateATMLead _leadDetails = _leadmgmtDAO.GetATMLeadCustInfo(ATMLeadId);
            return _leadDetails;
        }
        #endregion


        #region Private methods

        private void ValidateLeadMgmtSourceInput(LM_Master_SourceInputEntities leadMgmtSourceInputEntities)
        {
            string errorMesssage;
            if (leadMgmtSourceInputEntities == null)
            {
                errorMesssage = "Required Input Parameters Not Passed";
                throw new LeadManagementValidationException("Error-LGAdmin-100", errorMesssage);
            }
            if (string.IsNullOrEmpty(leadMgmtSourceInputEntities.Sourcename))
            {
                errorMesssage = "Source name is Required";
                throw new LeadManagementValidationException("Error-LGAdmin-101", errorMesssage);
            }
            if(leadMgmtSourceInputEntities.UserId <= 0)
            {
                errorMesssage = string.Format("User Id {0} is not valid user", leadMgmtSourceInputEntities.UserId);
                throw new LeadManagementValidationException("Error-LGAdmin-102", errorMesssage);
            }
        }


        private void ValidateLeadMgmtCategoryInput(LM_Master_CategoryInputEntities leadMgmtCategoryInputEntities)
        {
            string errorMesssage;
            if (leadMgmtCategoryInputEntities == null)
            {
                errorMesssage = "Required Input Parameters Not Passed";
                throw new LeadManagementValidationException("Error-LMWorkFlow-100", errorMesssage);
            }
            if (string.IsNullOrEmpty(leadMgmtCategoryInputEntities.LeadcategoryName))
            {
                errorMesssage = "Category name is Required";
                throw new LeadManagementValidationException("Error-LMWorkFlow-101", errorMesssage);
            }
            if (leadMgmtCategoryInputEntities.CreatedBy <= 0)
            {
                errorMesssage = string.Format("User Id {0} is not valid user", leadMgmtCategoryInputEntities.CreatedBy);
                throw new LeadManagementValidationException("Error-LMWorkFlow-102", errorMesssage);
            }
        }

        private void ValidateLeadMgmtStageInput(LM_Master_StageInputEntities leadMgmtStageInputEntities)
        {
            string errorMesssage;
            if (leadMgmtStageInputEntities == null)
            {
                errorMesssage = "Required Input Parameters Not Passed";
                throw new LeadManagementValidationException("Error-LMWorkFlow-100", errorMesssage);
            }
            if (string.IsNullOrEmpty(leadMgmtStageInputEntities.StageName))
            {
                errorMesssage = "Stage name is Required";
                throw new LeadManagementValidationException("Error-LMWorkFlow-101", errorMesssage);
            }
            if (leadMgmtStageInputEntities.CreatedBy <= 0)
            {
                errorMesssage = string.Format("User Id {0} is not valid user", leadMgmtStageInputEntities.CreatedBy);
                throw new LeadManagementValidationException("Error-LMWorkFlow-102", errorMesssage);
            }
        }

        private void ValidateLeadMgmtPriorityInput(LM_Master_PriorityInputEntities leadMgmtPriorityInputEntities)
        {
            string errorMesssage;
            if (leadMgmtPriorityInputEntities == null)
            {
                errorMesssage = "Required Input Parameters Not Passed";
                throw new LeadManagementValidationException("Error-LMWorkFlow-100", errorMesssage);
            }
            if (string.IsNullOrEmpty(leadMgmtPriorityInputEntities.StagePriority))
            {
                errorMesssage = "Priority is Required";
                throw new LeadManagementValidationException("Error-LMWorkFlow-101", errorMesssage);
            }
            if (leadMgmtPriorityInputEntities.CreatedBy <= 0)
            {
                errorMesssage = string.Format("User Id {0} is not valid user", leadMgmtPriorityInputEntities.CreatedBy);
                throw new LeadManagementValidationException("Error-LMWorkFlow-102", errorMesssage);
            }
        }

        private void ValidateLeadMgmtActionInput(LM_Master_ActionInputEntities leadMgmtActionInputEntities)
        {
            string errorMesssage;
            if (leadMgmtActionInputEntities == null)
            {
                errorMesssage = "Required Input Parameters Not Passed";
                throw new LeadManagementValidationException("Error-LMWorkFlow-100", errorMesssage);
            }
            if (string.IsNullOrEmpty(leadMgmtActionInputEntities.Description))
            {
                errorMesssage = "Action is Required";
                throw new LeadManagementValidationException("Error-LMWorkFlow-101", errorMesssage);
            }
            if (leadMgmtActionInputEntities.CreatedBy <= 0)
            {
                errorMesssage = string.Format("User Id {0} is not valid user", leadMgmtActionInputEntities.CreatedBy);
                throw new LeadManagementValidationException("Error-LMWorkFlow-102", errorMesssage);
            }
        }

        private void ValidateLeadMgmtCategoryStageMappingInput(LM_Master_CategoryStageMappingInputEntities leadMgmtCategoryStageMappingInputEntities)
        {
            string errorMesssage;
            if (leadMgmtCategoryStageMappingInputEntities == null)
            {
                errorMesssage = "Required Input Parameters Not Passed";
                throw new LeadManagementValidationException("Error-LMWorkFlow-100", errorMesssage);
            }
            if (leadMgmtCategoryStageMappingInputEntities.LeadCategoryId <= 0)
            {
                errorMesssage = "Category is Required";
                throw new LeadManagementValidationException("Error-LMWorkFlow-101", errorMesssage);
            }
            if (leadMgmtCategoryStageMappingInputEntities.LeadStageId <= 0)
            {
                errorMesssage = "Stage is Required";
                throw new LeadManagementValidationException("Error-LMWorkFlow-101", errorMesssage);
            }
            if (leadMgmtCategoryStageMappingInputEntities.SequenceId <= 0)
            {
                errorMesssage = "Sequence is Required";
                throw new LeadManagementValidationException("Error-LMWorkFlow-101", errorMesssage);
            }
            if (leadMgmtCategoryStageMappingInputEntities.CreatedBy <= 0)
            {
                errorMesssage = string.Format("User Id {0} is not valid user", leadMgmtCategoryStageMappingInputEntities.CreatedBy);
                throw new LeadManagementValidationException("Error-LMWorkFlow-102", errorMesssage);
            }
        }


        private void ValidateLeadMgmtActionStageMappingInput(LM_Master_ActionStageMappingInputEntities leadMgmtActionStageMappingInputEntities)
        {
            string errorMesssage;
            if (leadMgmtActionStageMappingInputEntities == null)
            {
                errorMesssage = "Required Input Parameters Not Passed";
                throw new LeadManagementValidationException("Error-LMWorkFlow-100", errorMesssage);
            }
            if (leadMgmtActionStageMappingInputEntities == null)
            {
                errorMesssage = "Required Input Parameters Not Passed";
                throw new LeadManagementValidationException("Error-LMWorkFlow-100", errorMesssage);
            }
            if (leadMgmtActionStageMappingInputEntities.LeadActionId <= 0)
            {
                errorMesssage = "Action is Required";
                throw new LeadManagementValidationException("Error-LMWorkFlow-101", errorMesssage);
            }
            if (leadMgmtActionStageMappingInputEntities.LeadStageId <= 0)
            {
                errorMesssage = "Stage is Required";
                throw new LeadManagementValidationException("Error-LMWorkFlow-101", errorMesssage);
            }
            if (leadMgmtActionStageMappingInputEntities.CreatedBy <= 0)
            {
                errorMesssage = string.Format("User Id {0} is not valid user", leadMgmtActionStageMappingInputEntities.CreatedBy);
                throw new LeadManagementValidationException("Error-LMWorkFlow-102", errorMesssage);
            }
        }






        #endregion
    }
}
