﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using LeadManagementEntities.Entities;
using System.Web.Http.Description;
using BSLeadManagementWorkflowAPI.App_Start;
using LeadManagementExceptionHandler;
using LeadManagementBO.Interfaces;
using System.Web;

namespace BSLeadManagementWorkflowAPI.Controllers
{
    public class LeadManagementTransactionController : LeadManagementWorkflowBaseApiController
    {
        private readonly ILeadMgmtBO _leadmgmtBO = null;
        public LeadManagementTransactionController(ILeadMgmtBO leadmgmtBO)
        {
            _leadmgmtBO = leadmgmtBO;
        }

        [HttpGet]
        [Route(RouteConstants.GetAllTickets)]
        [ResponseType(typeof(List<LM_Transaction_AllLeadTicket>))]
        public IHttpActionResult GetAllTickets()
        {
            List<LM_Transaction_AllLeadTicket> _alltickets = new List<LM_Transaction_AllLeadTicket>();
            _alltickets = _leadmgmtBO.GetAllLeadTickets();
            if (_alltickets == null)
            {
                return NotFound();
            }
            return Ok(_alltickets);
        }

        [HttpGet]
        [Route(RouteConstants.GetAllTicketsbyUserID)]
        [ResponseType(typeof(LM_Transaction_AllLeadDetails))]
        public IHttpActionResult GetAllTicketsbyUserID(long UserId)
        {
            List<LM_Transaction_AllLeadTicket> _alltickets = new List<LM_Transaction_AllLeadTicket>();
            _alltickets = _leadmgmtBO.GetAllTicketsbyUserID(UserId);
            if (_alltickets == null)
            {
                return NotFound();
            }
           
            return Ok(_alltickets);
        }

        [HttpGet]
        [Route(RouteConstants.GetAllLeads)]
        [ResponseType(typeof(LM_Transaction_AllLeadDetails))]
        public IHttpActionResult GetAllLeads()
        {
            LM_Transaction_AllLeadDetails _allLeadDetails = new LM_Transaction_AllLeadDetails();
            List<LM_Transaction_AllLeadTicket> _alltickets = new List<LM_Transaction_AllLeadTicket>();
            _alltickets = _leadmgmtBO.GetAllLeadTickets();
            if (_alltickets == null)
            {
                return NotFound();
            }
            _allLeadDetails.Stages = new List<Stages>();
            List<string> colors = new List<string>();
            colors.Add("bg-primary");
            colors.Add("bg-success");
            colors.Add("bg-info");
            colors.Add("bg-danger");
            colors.Add("bg-secondary");
            colors.Add("bg-dark");
            colors.Add("bg-light");
            colors.Add("bg-light");
            int i = 0;
            foreach (var item in _alltickets.OrderBy(x => x.LeadStageId).Select(x => x.LeadStageId).Distinct())
            {
                var stage = new Stages();
                stage.StageId = item;
                stage.Color = colors[i];
                stage.Tickets = new List<LM_Transaction_AllLeadTicket>();
                stage.Tickets = _alltickets.Where(x => x.LeadStageId == item).ToList();
                stage.StageName = stage.Tickets[0].Status;
                i++;

                _allLeadDetails.Stages.Add(stage);
            }
            return Ok(_allLeadDetails);
        }

        [HttpGet]
        [Route(RouteConstants.GetAllLeadsByUserId)]
        [ResponseType(typeof(LM_Transaction_AllLeadDetails))]
        public IHttpActionResult GetAllLeadsByUserId(long UserId)
        {
            LM_Transaction_AllLeadDetails _allLeadDetails = new LM_Transaction_AllLeadDetails();
            List<LM_Transaction_AllLeadTicket> _alltickets = new List<LM_Transaction_AllLeadTicket>();
            _alltickets = _leadmgmtBO.GetAllLeadsByUserId(UserId);
            if (_alltickets == null)
            {
                return NotFound();
            }
            _allLeadDetails.Stages = new List<Stages>();
            List<string> colors = new List<string>();
            colors.Add("bg-primary");
            colors.Add("bg-success");
            colors.Add("bg-info");
            colors.Add("bg-danger");
            colors.Add("bg-secondary");
            colors.Add("bg-dark");
            colors.Add("bg-light");
            colors.Add("bg-light");
            int i = 0;
            foreach (var item in _alltickets.OrderBy(x => x.LeadStageId).Select(x => x.LeadStageId).Distinct())
            {
                var stage = new Stages();
                stage.StageId = item;
                stage.Color = colors[i];
                stage.Tickets = new List<LM_Transaction_AllLeadTicket>();
                stage.Tickets = _alltickets.Where(x => x.LeadStageId == item).ToList();
                stage.StageName = stage.Tickets[0].Status;
                i++;

                _allLeadDetails.Stages.Add(stage);
            }
            return Ok(_allLeadDetails);
        }
        [HttpGet]
        [Route(RouteConstants.GetAllLead)]
        [ResponseType(typeof(List<LM_Transaction_AllLeadTicket>))]
        public IHttpActionResult GetAllLead()
        {
            List<LM_Transaction_AllLeadTicket> _userSource = new List<LM_Transaction_AllLeadTicket>();
            _userSource = _leadmgmtBO.GetAllLead();
            if (_userSource == null)
            {
                return NotFound();
            }
            return Ok(_userSource);
        }
        [HttpGet]
        [Route(RouteConstants.GetAllTicketsByLeadId)]
        [ResponseType(typeof(List<LM_Transaction_AllLeadTicket>))]
        public IHttpActionResult GetAllTicketsByLeadId(long LeadId)
        {
            List<LM_Transaction_AllLeadTicket> _userSource = new List<LM_Transaction_AllLeadTicket>();
            _userSource = _leadmgmtBO.GetAllTicketsByLeadId(LeadId);
            if (_userSource == null)
            {
                return NotFound();
            }
            return Ok(_userSource);
        }

        [HttpGet]
        [Route(RouteConstants.GetLiveTicket)]
        [ResponseType(typeof(LM_Transaction_LiveTicket))]
        public IHttpActionResult GetLiveTicket(long ticketId)
        {
            LM_Transaction_LiveTicket _liveticket = new LM_Transaction_LiveTicket();
            _liveticket = _leadmgmtBO.GetLiveTicket(ticketId);
            if (_liveticket == null)
            {
                return NotFound();
            }
            return Ok(_liveticket);
        }

        [HttpGet]
        [Route(RouteConstants.GetLeadComments)]
        [ResponseType(typeof(List<LM_Transaction_TicketComments>))]
        public IHttpActionResult GetLeadComments(long leadId)
        {
            List<LM_Transaction_TicketComments> _comments = new List<LM_Transaction_TicketComments>();
            _comments = _leadmgmtBO.GetLeadComments(leadId);
            if (_comments == null)
            {
                return NotFound();
            }
            return Ok(_comments);
        }

        [HttpPost]
        [Route(RouteConstants.InsertLeadTicket)]
        [ResponseType(typeof(bool))]
        public IHttpActionResult InsertLeadTicket(LM_Transaction_LeadInputEntities leadMgmtLeadTicketInputEntities)
        {
            return Ok(_leadmgmtBO.InsertLeadTicket(leadMgmtLeadTicketInputEntities));
        }

        [HttpPost]
        [Route(RouteConstants.ModifyLeadTicket)]
        [ResponseType(typeof(List<LM_LeadActions>))]
        public IHttpActionResult ModifyLeadTicket(LM_Transaction_TicketModifyInputEntities ticketEntities)
        {
            return Ok(_leadmgmtBO.ModifyLeadTicket(ticketEntities));
        }

        [HttpGet]
        [Route(RouteConstants.GetLeadTimeLines)]
        [ResponseType(typeof(List<LM_LeadActions>))]
        public IHttpActionResult GetLeadTimeLines(long LeadId)
        {
            return Ok(_leadmgmtBO.GetLeadTimeLines(LeadId));
        }

        [HttpGet]
        [Route(RouteConstants.GetLeadStakeHolders)]
        [ResponseType(typeof(List<long>))]
        public IHttpActionResult GetLeadStakeHolders(long LeadId)
        {
            return Ok(_leadmgmtBO.GetLeadStakeHolders(LeadId));
        }

        [HttpGet]
        [Route(RouteConstants.GetCrossSellingLeads)]
        [ResponseType(typeof(List<LM_Transaction_CrossSelling>))]
        public IHttpActionResult GetCrossSellingLeads()
        {
            List<LM_Transaction_CrossSelling> _crossSelling = new List<LM_Transaction_CrossSelling>();
            _crossSelling = _leadmgmtBO.GetCrossSellingLeads();
            if (_crossSelling == null)
            {
                return NotFound();
            }
            return Ok(_crossSelling);
        }

        [HttpGet]
        [Route(RouteConstants.GetCrossSellingCustInfo)]
        [ResponseType(typeof(LM_Transaction_CreateCrossSellingLead))]
        public IHttpActionResult GetCrossSellingCustInfo(string cif_id)
        {
            return Ok(_leadmgmtBO.GetCrossSellingCustInfo(cif_id));
        }

        [HttpGet]
        [Route(RouteConstants.GetATMLeads)]
        [ResponseType(typeof(List<LM_Transaction_ATM>))]
        public IHttpActionResult GetATMLeads()
        {
            List<LM_Transaction_ATM> _aTMs = new List<LM_Transaction_ATM>();
            _aTMs = _leadmgmtBO.GetATMLeads();
            if (_aTMs == null)
            {
                return NotFound();
            }
            return Ok(_aTMs);
        }


        [HttpGet]
        [Route(RouteConstants.GetATMLeadCustInfo)]
        [ResponseType(typeof(LM_Transaction_CreateATMLead))]
        public IHttpActionResult GetATMLeadCustInfo(long ATMLeadId)
        {
            return Ok(_leadmgmtBO.GetATMLeadCustInfo(ATMLeadId));
        }
    }
}
