﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using LeadManagementEntities.Entities;
using System.Web.Http.Description;
using BSLeadManagementWorkflowAPI.App_Start;
using LeadManagementExceptionHandler;
using LeadManagementBO.Interfaces;

namespace BSLeadManagementWorkflowAPI.Controllers
{
   // [Authorize]
    public class LeadManagementMasterController : LeadManagementWorkflowBaseApiController
    {
        private readonly ILeadMgmtBO _leadmgmtBO = null;
        public LeadManagementMasterController(ILeadMgmtBO leadmgmtBO)
        {
            _leadmgmtBO = leadmgmtBO;
        }

        [HttpGet]
        [Route(RouteConstants.GetLeadSource)]
        [ResponseType(typeof(List<LM_Master_SourceEntities>))]
        public IHttpActionResult GetLeadSource()
        {
            List<LM_Master_SourceEntities> _leadSource = new List<LM_Master_SourceEntities>();
            _leadSource = _leadmgmtBO.GetLeadSource();
            if (_leadSource == null)
            {
                return NotFound();
            }
            return Ok(_leadSource);
        }


        [HttpPost]
        [Route(RouteConstants.InsertLeadSource)]
        [ResponseType(typeof(bool))]
        public IHttpActionResult InsertLeadSource(LM_Master_SourceInputEntities leadMgmtSourceInputEntities)
        {
            return Ok(_leadmgmtBO.InsertLeadSource(leadMgmtSourceInputEntities));
        }


        [HttpGet]
        [Route(RouteConstants.GetLeadCategoryDetails)]
        [ResponseType(typeof(List<LM_Master_CategoryEntities>))]
        public IHttpActionResult GetLeadCategoryDetails()
        {
            List<LM_Master_CategoryEntities> _leadCategory = new List<LM_Master_CategoryEntities>();
            _leadCategory = _leadmgmtBO.GetLeadCategoryDetails();
            if (_leadCategory == null)
            {
                return NotFound();
            }
            return Ok(_leadCategory);
        }


        [HttpPost]
        [Route(RouteConstants.InsertLeadCategory)]
        [ResponseType(typeof(bool))]
        public IHttpActionResult InsertLeadCategory(LM_Master_CategoryInputEntities leadMgmtCategoryInputEntities)
        {
            return Ok(_leadmgmtBO.InsertLeadCategory(leadMgmtCategoryInputEntities));
        }


        [HttpGet]
        [Route(RouteConstants.GetLeadStageDetails)]
        [ResponseType(typeof(List<LM_Master_StageEntities>))]
        public IHttpActionResult GetLeadStageDetails()
        {
            List<LM_Master_StageEntities> _leadStage = new List<LM_Master_StageEntities>();
            _leadStage = _leadmgmtBO.GetLeadStageDetails();
            if (_leadStage == null)
            {
                return NotFound();
            }
            return Ok(_leadStage);
        }


        [HttpPost]
        [Route(RouteConstants.InsertLeadStage)]
        [ResponseType(typeof(bool))]
        public IHttpActionResult InsertLeadStage(LM_Master_StageInputEntities leadMgmtStageInputEntities)
        {
            return Ok(_leadmgmtBO.InsertLeadStage(leadMgmtStageInputEntities));
        }

        [HttpGet]
        [Route(RouteConstants.GetLeadPriorityDetails)]
        [ResponseType(typeof(List<LM_Master_PriorityEntities>))]
        public IHttpActionResult GetLeadPriorityDetails()
        {
            List<LM_Master_PriorityEntities> _leadPriority = new List<LM_Master_PriorityEntities>();
            _leadPriority = _leadmgmtBO.GetLeadPriorityDetails();
            if (_leadPriority == null)
            {
                return NotFound();
            }
            return Ok(_leadPriority);
        }


        [HttpPost]
        [Route(RouteConstants.InsertLeadPriority)]
        [ResponseType(typeof(bool))]
        public IHttpActionResult InsertLeadPriority(LM_Master_PriorityInputEntities leadMgmtPriorityInputEntities)
        {
            return Ok(_leadmgmtBO.InsertLeadPriority(leadMgmtPriorityInputEntities));
        }


        [HttpGet]
        [Route(RouteConstants.GetLeadActionDetails)]
        [ResponseType(typeof(List<LM_Master_ActionEntities>))]
        public IHttpActionResult GetLeadActionDetails()
        {
            List<LM_Master_ActionEntities> _leadAction = new List<LM_Master_ActionEntities>();
            _leadAction = _leadmgmtBO.GetLeadActionDetails();
            if (_leadAction == null)
            {
                return NotFound();
            }
            return Ok(_leadAction);
        }


        [HttpPost]
        [Route(RouteConstants.InsertLeadAction)]
        [ResponseType(typeof(bool))]
        public IHttpActionResult InsertLeadAction(LM_Master_ActionInputEntities leadMgmtActionInputEntities)
        {
            return Ok(_leadmgmtBO.InsertLeadAction(leadMgmtActionInputEntities));
        }


        [HttpGet]
        [Route(RouteConstants.GetLeadCategoryStageMappingDetails)]
        [ResponseType(typeof(List<LM_Master_CategoryStageMappingEntities>))]
        public IHttpActionResult GetLeadCategoryStageMappingDetails()
        {
            List<LM_Master_CategoryStageMappingEntities> _leadCategoryStageMapping = new List<LM_Master_CategoryStageMappingEntities>();
            _leadCategoryStageMapping = _leadmgmtBO.GetLeadCategoryStageMappingDetails();
            if (_leadCategoryStageMapping == null)
            {
                return NotFound();
            }
            return Ok(_leadCategoryStageMapping);
        }

        [HttpGet]
        [Route(RouteConstants.GetLeadCategoryStageMapping)]
        [ResponseType(typeof(List<LM_Master_CategoryStageMappingEntities>))]
        public IHttpActionResult GetLeadCategoryStageMapping(long categoryId)
        {
            List<LM_Master_CategoryStageMappingEntities> _leadCategoryStageMapping = new List<LM_Master_CategoryStageMappingEntities>();
            _leadCategoryStageMapping = _leadmgmtBO.GetLeadCategoryStageMapping(categoryId);
            if (_leadCategoryStageMapping == null)
            {
                return NotFound();
            }
            return Ok(_leadCategoryStageMapping);
        }


        [HttpPost]
        [Route(RouteConstants.InsertLeadCategoryStageMapping)]
        [ResponseType(typeof(bool))]
        public IHttpActionResult InsertLeadCategoryStageMapping(LM_Master_CategoryStageMappingInputEntities leadMgmtCategoryStageMappingInputEntities)
        {
            return Ok(_leadmgmtBO.InsertLeadCategoryStageMapping(leadMgmtCategoryStageMappingInputEntities));
        }

        [HttpGet]
        [Route(RouteConstants.GetLeadActionStageMappingDetails)]
        [ResponseType(typeof(List<LM_Master_ActionStageMappingEntities>))]
        public IHttpActionResult GetLeadActionStageMappingDetails()
        {
            List<LM_Master_ActionStageMappingEntities> _leadActionStageMapping = new List<LM_Master_ActionStageMappingEntities>();
            _leadActionStageMapping = _leadmgmtBO.GetLeadActionStageMappingDetails();
            if (_leadActionStageMapping == null)
            {
                return NotFound();
            }
            return Ok(_leadActionStageMapping);
        }


        [HttpPost]
        [Route(RouteConstants.InsertLeadActionStageMapping)]
        [ResponseType(typeof(bool))]
        public IHttpActionResult InsertLeadActionStageMapping(LM_Master_ActionStageMappingInputEntities leadMgmtActionStageMappingInputEntities)
        {
            return Ok(_leadmgmtBO.InsertLeadActionStageMapping(leadMgmtActionStageMappingInputEntities));
        }

    }
}
