﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BSLeadManagementWorkflowAPI.App_Start
{
    public static class RouteConstants
    {
        public const string GetLeadSource = "~/api/LeadManagement/GetLeadSource";
        public const string InsertLeadSource = "~/api/LeadManagement/InsertLeadSource";


        public const string GetLeadCategoryDetails = "~/api/LeadManagement/GetLeadCategoryDetails";
        public const string InsertLeadCategory = "~/api/LeadManagement/InsertLeadCategory";

        public const string GetLeadStageDetails = "~/api/LeadManagement/GetLeadStageDetails";
        public const string InsertLeadStage = "~/api/LeadManagement/InsertLeadStage";

        public const string GetLeadPriorityDetails = "~/api/LeadManagement/GetLeadPriorityDetails";
        public const string InsertLeadPriority = "~/api/LeadManagement/InsertLeadPriority";

        public const string GetLeadActionDetails = "~/api/LeadManagement/GetLeadActionDetails";
        public const string InsertLeadAction = "~/api/LeadManagement/InsertLeadAction";

        public const string GetLeadCategoryStageMappingDetails = "~/api/LeadManagement/GetLeadCategoryStageMappingDetails";
        public const string GetLeadCategoryStageMapping = "~/api/LeadManagement/GetLeadCategoryStageMapping";
        public const string InsertLeadCategoryStageMapping = "~/api/LeadManagement/InsertLeadCategoryStageMapping";

        public const string GetLeadActionStageMappingDetails = "~/api/LeadManagement/GetLeadActionStageMappingDetails";
        public const string InsertLeadActionStageMapping = "~/api/LeadManagement/InsertLeadActionStageMapping";

        //Transaction
        public const string GetLiveTicket = "~/api/LeadManagementTransaction/GetLiveTicket";
        public const string GetLeadComments = "~/api/LeadManagementTransaction/GetLeadComments";
        public const string GetAllTicketsbyUserID = "~/api/LeadManagementTransaction/GetAllTicketsbyUserID";
        public const string GetAllTickets = "~/api/LeadManagementTransaction/GetAllTickets";
        public const string GetAllTicketsByLeadId = "~/api/LeadManagementTransaction/GetAllTicketsByLeadId";
        public const string GetAllLeadsByUserId = "~/api/LeadManagementTransaction/GetAllLeadsByUserId";
        public const string GetAllLeads = "~/api/LeadManagementTransaction/GetAllLeads";
        public const string InsertLeadTicket = "~/api/LeadManagementTransaction/InsertLeadTicket";
        public const string ModifyLeadTicket = "~/api/LeadManagementTransaction/ModifyLeadTicket";
        public const string GetLeadTimeLines = "~/api/LeadManagementTransaction/GetLeadTimeLines";
        public const string GetLeadStakeHolders = "~/api/LeadManagementTransaction/GetLeadStakeHolders";
        public const string GetAllLead = "~/api/LeadManagementTransaction/GetAllLead";
        public const string GetCrossSellingLeads = "~/api/LeadManagementTransaction/GetCrossSellingLeads";
        public const string GetCrossSellingCustInfo = "~/api/LeadManagementTransaction/GetCrossSellingCustInfo";
        public const string GetATMLeads = "~/api/LeadManagementTransaction/GetATMLeads";
        public const string GetATMLeadCustInfo = "~/api/LeadManagementTransaction/GetATMLeadCustInfo";
    }
}