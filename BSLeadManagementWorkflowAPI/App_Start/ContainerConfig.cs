﻿using System.Web.Http;
using Autofac;
using Autofac.Integration.WebApi;
using System.Reflection;
using LeadManagementDAO;
using LeadManagementDAO.ILeadMgmt;
using LeadManagementBO;
using LeadManagementBO.Interfaces;

namespace BSLeadManagementWorkflowAPI.App_Start
{
    /// <summary>
    /// This is used for dependency injection.
    /// This is the prefered way when we have a test driven environnement.
    /// used in this project with a future perspective to provide automated test cases on a CI/CD pipeline if decided for a container based deployment
    /// </summary>
    public static class ContainerConfig
    {
        public static IContainer RegisterAutofac()
        {
            var builder = new ContainerBuilder();
            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());
            builder.RegisterType<LeadMgmtBO>().As<ILeadMgmtBO>();
            builder.RegisterType<LeadMgmtDAO>().As<ILeadMgmtDAO>();
            var container = builder.Build();

            var webApiResolver = new AutofacWebApiDependencyResolver(container);
            GlobalConfiguration.Configuration.DependencyResolver = webApiResolver;
            return container;
        }
    }
}