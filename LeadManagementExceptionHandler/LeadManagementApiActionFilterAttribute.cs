﻿using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace LeadManagementExceptionHandler
{
    public class LeadManagementApiActionFilterAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            //setting up the controller for future use to get the tokens or identify the user
            LeadManagementWorkflowBaseApiController controller = (LeadManagementWorkflowBaseApiController)actionContext.ControllerContext.Controller;

            //string user = Convert.ToString(controller.User);
        }
    }
}
