﻿using LeadManagementEntities.Error;
using LeadManagementExceptionHandler.Entities;
using LeadManagementExceptionHandler.Helper;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace LeadManagementExceptionHandler
{
    public class LeadManagementApiExceptionFilterAttribute : ExceptionFilterAttribute
    {
        #region Public Methods
        public LeadManagementApiExceptionFilterAttribute()
        {
        }

        public override void OnException(HttpActionExecutedContext actionExecutedContext)
        {
            try
            {
                if (actionExecutedContext.Exception != null)
                {
                    string controllerWarning = string.Empty;
                    var actionDesc = ((ReflectedHttpActionDescriptor)actionExecutedContext.ActionContext.ActionDescriptor);
                    var requestHeader = actionExecutedContext.ActionContext.Request.Headers;


                    IHttpController controller = actionExecutedContext.ActionContext.ControllerContext.Controller;
                    string apiMethodArguments = JsonConvert.SerializeObject(actionExecutedContext.ActionContext.ActionArguments);

                    string consolidatedErrorMessage = string.Format("Controller: {0};\nAction: {1};\nParams: {2};\nTrace: {3}",
                        actionDesc.ControllerDescriptor.ControllerName, actionDesc.ActionName, apiMethodArguments,
                        actionExecutedContext.Exception.StackTrace);

                    Exception ex = new Exception(consolidatedErrorMessage, actionExecutedContext.Exception);
                    Exception innerMostException = GetInnerMostException(actionExecutedContext.Exception);



                    actionExecutedContext.Response = GetResponseDataForUI(actionExecutedContext.Exception, requestHeader);
                }
            }
            catch (Exception)
            {

                actionExecutedContext.Response = GetResponseDataForUI(actionExecutedContext.Exception, actionExecutedContext.ActionContext.Request.Headers);
            }
        }
        #endregion

        #region Private Methods
        private HttpResponseMessage GetResponseDataForUI(Exception ex, HttpRequestHeaders httpRequestHeader)
        {
            string errorContextJson;
            LeadManagementErrors errorContext = null;
            HttpStatusCode httpStatusCode;

            if (ex is LeadManagementValidationException)
            {
                LeadManagementValidationException leadManagementException = ex as LeadManagementValidationException;
                errorContext = new LeadManagementErrors
                {
                    ErrorCode = leadManagementException.ErrorCode,
                    ErrorMessage = leadManagementException.Message
                };

                httpStatusCode = HttpErrorResponseHandler.GetHttpErrorResponseCode(leadManagementException);
            }
            else
            {
                errorContext = new LeadManagementErrors
                {
                    ErrorCode = "Unhandled Exception",
                    ErrorMessage = ex.Message
                };

                httpStatusCode = HttpStatusCode.InternalServerError;
            }

            if (httpRequestHeader.Contains("GetFullExceptionWithTrace") &&
                Convert.ToString(httpRequestHeader.GetValues("GetFullExceptionWithTrace").FirstOrDefault()).ToUpper() == "TRUE")
            {
                string fullException = GetFullExceptionWithTrace(ex);
                errorContext.ErrorMessage = errorContext.ErrorMessage + Environment.NewLine + "\"Trace\" : " + fullException;
            }

            errorContextJson = JsonConvert.SerializeObject(errorContext);

            var resp = new HttpResponseMessage(httpStatusCode)
            {
                Content = new StringContent(errorContextJson, Encoding.UTF8, "application/json")
            };

            return resp;
        }

        private string GetFullExceptionWithTrace(Exception ex, int level = 0)
        {
            string exceptionWithTrace;
            StringBuilder sb = new StringBuilder();

            level++;
            if (ex.InnerException != null)
            {
                sb.AppendLine(GetFullExceptionWithTrace(ex.InnerException, level));
            }
            sb.AppendLine("\n------------- Ex Level: " + (level));
            sb.AppendLine(ex.Message);
            sb.AppendLine(ex.StackTrace);
            sb.AppendLine();
            exceptionWithTrace = sb.ToString();
            sb.Clear();
            sb = null;
            return exceptionWithTrace;
        }

        private Exception GetInnerMostException(Exception ex)
        {
            if (ex.InnerException != null)
            {
                return GetInnerMostException(ex.InnerException);
            }
            else
            {
                return ex;
            }
        }
        #endregion
    }
}
