﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeadManagementEntities.Entities
{
    public class LM_Transaction_CreateATMLead
    {
        public string Mobile { get; set; }
        public string Product { get; set; }
        public long LeadSourceId { get; set; }
        public long LeadCategoryId { get; set; }
        public long ProductId { get; set; }

    }
}
