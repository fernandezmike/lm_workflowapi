﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeadManagementEntities.Entities
{
    public class LM_Transaction_TicketAttachmentInfo
    {
        public long AttachmentId { get; set; }
        public Nullable<long> TicketId { get; set; }
        public string Path { get; set; }
    }
}
