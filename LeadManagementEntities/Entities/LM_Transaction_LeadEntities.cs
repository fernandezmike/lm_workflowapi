﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeadManagementEntities.Entities
{
    public class LM_Transaction_LeadEntities
    {
        public long LeadId { get; set; }
        public string LeadDescription { get; set; }
        public Nullable<int> LeadSourceId { get; set; }
        public string CustomerName { get; set; }
        public string CustomerEmail { get; set; }
        public string CustomerContactNo { get; set; }
        public Nullable<bool> IsExistingCustomer { get; set; }
        public Nullable<long> CIFID { get; set; }
        public Nullable<int> ProductId { get; set; }
        public Nullable<int> LeadScore { get; set; }
        public Nullable<long> LeadRevenue { get; set; }
    }
}
