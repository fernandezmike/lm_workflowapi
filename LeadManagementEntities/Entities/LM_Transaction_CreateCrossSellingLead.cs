﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeadManagementEntities.Entities
{
    public class LM_Transaction_CreateCrossSellingLead
    {
        public string LeadTitle { get; set; }
        public string CIF_ID { get; set; }
        public double Acct_no { get; set; }
        public string Cust_Name { get; set; }
        public string Mobile_No { get; set; }
        public string Branch_Name { get; set; }
        public string Lead_Source { get; set; }
        public string Product { get; set; }
    }
}
