﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeadManagementEntities.Entities
{
    public class LM_Master_PriorityEntities
    {
        public int LeadPriorityId { get; set; }
        public string StagePriority { get; set; }
        public string Description { get; set; }
    }
}
