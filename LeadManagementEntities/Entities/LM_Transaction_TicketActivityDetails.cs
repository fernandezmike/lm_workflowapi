﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeadManagementEntities.Entities
{
    public class LM_Transaction_TicketActivityDetails
    {
        public long LeadActionId { get; set; }
        public Nullable<long> TicketId { get; set; }
        public Nullable<long> LeadId { get; set; }
        public Nullable<int> LeadCategoryId { get; set; }
        public Nullable<long> LeadStageId { get; set; }
    }
}
