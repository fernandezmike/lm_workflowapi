﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeadManagementEntities.Entities
{
    public class LM_Master_SourceInputEntities
    {
        public string Sourcename { get; set; }

        public long UserId { get; set; }
    }
}
