﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeadManagementEntities.Entities
{
    public class LM_Master_StageEntities
    {
        public long LeadStageId { get; set; }
        public string StageName { get; set; }
        public string Description { get; set; }
    }
}
