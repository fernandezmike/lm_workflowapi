﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeadManagementEntities.Entities
{
    public class LM_Transaction_CrossSelling
    {
        public string CIF_ID { get; set; }
        public double Acct_no { get; set; }
        public string Cust_Name { get; set; }
        public string Mobile_No { get; set; }
        public string Branch_Name { get; set; }
        public string Existing_OR_New { get; set; }
        public string Product { get; set; }
    }
}
