﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeadManagementEntities.Entities
{
    public class LM_Master_ActionStageMappingEntities
    {
        public Nullable<long> LeadStageId { get; set; }
        public Nullable<long> LeadActionId { get; set; }
        public Nullable<int> SequenceId { get; set; }

        public LM_Master_ActionEntities LeadMgmtActionEntities { get; set; }
        public LM_Master_StageEntities LeadMgmtStageEntities { get; set; }
    }
}
