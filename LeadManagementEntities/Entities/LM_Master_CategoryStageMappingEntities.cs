﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeadManagementEntities.Entities
{
    public class LM_Master_CategoryStageMappingEntities
    {
        public Nullable<long> LeadStageId { get; set; }
        public Nullable<long> LeadCategoryId { get; set; }
        public Nullable<int> SequenceId { get; set; }

        public LM_Master_CategoryEntities LeadMgmtCategoryEntities { get; set; }
        public LM_Master_StageEntities LeadMgmtStageEntities { get; set; }
    }
}
