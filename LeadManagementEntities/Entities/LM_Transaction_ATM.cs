﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeadManagementEntities.Entities
{
    public class LM_Transaction_ATM
    {
        public long ATMLeadId { get; set; }
        public string CardNo { get; set; }
        public string Mobile { get; set; }
        public string Product { get; set; }
    }
}
