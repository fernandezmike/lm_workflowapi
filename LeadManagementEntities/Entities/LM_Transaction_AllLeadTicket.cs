﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeadManagementEntities.Entities
{
    public class LM_Transaction_AllLeadTicket
    {
        public long LeadId { get; set; }
        public string LeadDescription { get; set; }
        public long TicketId { get; set; }
        public long LeadStageId { get; set; }
        public string LeadCategoryName { get; set; }
        public long ProductId { get; set; }
        public string ProductName { get; set; }
        public bool IsActive { get; set; }
        public long CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public String Status { get; set; }
        public Nullable<long> TicketAssigineeId { get; set; }
        public String TicketAssiginee { get; set; }
        public String Sourcename { get; set; }
        public int Score { get; set; }
        public Nullable<System.DateTime> UpdatedOn { get; set; } 
    }
    public class LM_Transaction_AllLeadDetails
    {
        public List<Stages> Stages { get; set; }
    }
    public class Stages
    {
        public String Color { get; set; }

        public String StageName { get; set; }
        public long StageId { get; set; }
        public List<LM_Transaction_AllLeadTicket> Tickets { get; set; }
    }
}
