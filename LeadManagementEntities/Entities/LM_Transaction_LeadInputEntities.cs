﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeadManagementEntities.Entities
{
    public class LM_Transaction_LeadInputEntities
    {
        public string LeadDescription { get; set; }
        public Nullable<int> LeadSourceId { get; set; }
        public string CustomerName { get; set; }
        public string CustomerEmail { get; set; }
        public string CustomerContactNo { get; set; }
        public Nullable<bool> IsExistingCustomer { get; set; }
        public string CIFID { get; set; }
        public Nullable<int> ProductId { get; set; }
        public Nullable<int> RegionId { get; set; }

        public Nullable<long> CategoryId { get; set; }
        public Nullable<long> StageId { get; set; }
        public string AttachmentPath { get; set; }
        public string Comments { get; set; }
        public Nullable<long> CreatedBy { get; set; }
        public Nullable<long> TicketAssigineeId { get; set; }
        public Nullable<bool> IsCrossLead { get; set; }
        public Nullable<long> ATMLeadId { get; set; }
    }
}
