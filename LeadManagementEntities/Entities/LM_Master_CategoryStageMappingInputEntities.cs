﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeadManagementEntities.Entities
{
    public class LM_Master_CategoryStageMappingInputEntities
    {
        public Nullable<long> LeadStageId { get; set; }
        public Nullable<long> LeadCategoryId { get; set; }
        public Nullable<int> SequenceId { get; set; }
        public long? CreatedBy { get; set; }

    }
}
