﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeadManagementEntities.Entities
{
    public class LM_Master_SourceEntities
    {
        public int LeadSourceId { get; set; }
        public string Sourcename { get; set; }
    }
}
