﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeadManagementEntities.Entities
{
    public class LM_Transaction_TicketModifyInputEntities
    {
        public long TicketId { get; set; }
        public long LeadStageId { get; set; }
        public long LeadId { get; set; }
        public long CreatedBy { get; set; }
        public string Comments { get; set; }
        public int LeadScore { get; set; }
        public int Revenue { get; set; }
        public int ProductId { get; set; }
        public int AssigineeId { get; set; }
        public int LeadCategoryId { get; set; }
        public long BranchID { get; set; }
        public string BranchName { get; set; }
        public bool IsActive { get; set; }

    }
}
