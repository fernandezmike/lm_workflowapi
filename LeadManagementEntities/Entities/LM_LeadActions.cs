﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeadManagementEntities.Entities
{
   public class LM_LeadActions
    {
        public long LeadId { get; set; }
        public long TicketId { get; set; }
        public string LeadAction { get; set; }
        public DateTime CreatedOn { get; set; }
        public long CreatedBy { get; set; }
    }
}
