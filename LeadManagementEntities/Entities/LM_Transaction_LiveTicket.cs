﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeadManagementEntities.Entities
{
    public class LM_Transaction_LiveTicket
    {

        public long TicketId { get; set; }
        public long CategoryId { get; set; }
        public long LeadStageId { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public String Status { get; set; }
        public Nullable<long> TicketAssigineeId { get; set; }
        public String TicketAssiginee { get; set; }
        public String Source { get; set; }
        public string LeadCategoryName { get; set; }
        public Nullable<System.DateTime> UpdatedOn { get; set; }
        public long LeadId { get; set; }
        public string LeadDescription { get; set; }
        public string LeadStage { get; set; }
        public string CustomerName { get; set; }
        public string CustomerEmail { get; set; }
        public string CustomerContactNo { get; set; }
        public Nullable<bool> IsExistingCustomer { get; set; }
        public Nullable<long> CIFID { get; set; }
        public Nullable<int> ProductId { get; set; }
        public string Product { get; set; }
        public Nullable<int> LeadScore { get; set; }
        public Nullable<long> LeadRevenue { get; set; }
        public long CreatedBy { get; set; }
        public string Path { get; set; }
        public int CommentCount { get; set; }
        public long BranchId { get; set; }
        public string BranchName { get; set; }
        public bool IsActive { get; set; }
    }
}
