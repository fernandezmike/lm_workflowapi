﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeadManagementEntities.Entities
{
    public class LM_Master_CategoryEntities
    {
        public int LeadCategoryId { get; set; }
        public string LeadcategoryName { get; set; }
        public bool? IsActive { get; set; }
        public string Description { get; set; }
        public System.DateTime? CreatedOn { get; set; }
        public long? CreatedBy { get; set; }
        public System.DateTime? UpdatedOn { get; set; }
        public long? UpdatedBy { get; set; } 
    }
}
