﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeadManagementEntities.Entities
{
    public class LM_Transaction_StakeHolderDetails
    {
        public long StakeHolderDetailID { get; set; }
        public Nullable<long> LeadId { get; set; }
        public Nullable<long> StakeHolderId { get; set; }
        public Nullable<int> StakeHolderStatus { get; set; }
    }
}
