﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeadManagementEntities.Entities
{
    public class LM_Master_CategoryInputEntities
    {
        public string LeadcategoryName { get; set; }
        public string Description { get; set; }
        public long? CreatedBy { get; set; }
    }
}
