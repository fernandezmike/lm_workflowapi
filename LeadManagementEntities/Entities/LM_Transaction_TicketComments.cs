﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeadManagementEntities.Entities
{
    public class LM_Transaction_TicketComments
    {
        public long CommentId { get; set; }
        public long CreatedBy { get; set; }
        public Nullable<long> LeadId { get; set; }
        public string Comments { get; set; }
        public string UserName { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
    }
}
