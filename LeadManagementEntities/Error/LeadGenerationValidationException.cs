﻿using System;

namespace LeadManagementEntities.Error
{
    public class LeadManagementValidationException : Exception
    {
        /// <summary>
        /// Localized string for error message.
        /// </summary>
        public string ErrorCode { get; set; }
        public LeadManagementValidationException(string errorCode, string errorMessage) : base(errorMessage)
        {
            ErrorCode = errorCode;
        }
    }
}
