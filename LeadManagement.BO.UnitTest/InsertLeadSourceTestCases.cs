﻿using Autofac.Extras.Moq;
using LeadManagementDAO.ILeadMgmt;
using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Moq;
using LeadManagementEntities.Entities;
using LeadManagementBO;
using Newtonsoft.Json;
using LeadManagement.BO.UnitTest.TestCaseCreator;
using LeadManagementEntities.Error;
using System.Collections.Generic;

namespace LeadManagement.BO.UnitTest
{
    public class InsertLeadSourceTestCases
    {
        #region Private variables

        private bool _success = true;
        private LM_Master_SourceInputEntities leadMgmtSourceInputEntities = new LM_Master_SourceInputEntities
        {
            Sourcename = "Test",
            UserId = 1
        };

        #endregion

        #region Public Methods

        #region Positive Test Cases

        [Fact]
        public void TestGetLeadSource_ShouldReturn_Success()
        {
            using (var mock = AutoMock.GetLoose())
            {
                mock.Mock<ILeadMgmtDAO>().Setup(x => x.GetLeadSource()).Returns(GetLeadMgmtSourceList());
                var _leadmgmtBO = mock.Create<LeadMgmtBO>();
                var _actual = _leadmgmtBO.GetLeadSource();
                Assert.Equal(JsonConvert.SerializeObject(GetLeadMgmtSourceList()), JsonConvert.SerializeObject(_actual));
            }
        }
        [Fact]
        public void TestInsertLeadSource_ShouldReturn_Success()
        {
            using(var mock = AutoMock.GetLoose())
            {
                mock.Mock<ILeadMgmtDAO>().Setup(x => x.InsertLeadSource(It.IsAny<LM_Master_SourceInputEntities>())).Returns(_success);
                var _leadmgmtBO = mock.Create<LeadMgmtBO>();
                var _actual = _leadmgmtBO.InsertLeadSource(leadMgmtSourceInputEntities);
                Assert.Equal(_success, _actual);
            }
        }

        #endregion

        #region Negative Test Cases

        [Theory]
        [MemberData(nameof(InsertLeadSourceNegativeTestCaseCreator.InsertLeadSourceValidationIndex),
            MemberType = typeof(TestCaseCreator.InsertLeadSourceNegativeTestCaseCreator))]
        public void InsertLeadSource_ValidationTest(int input)
        {
            string _expected = string.Empty;

            var _request = InsertLeadSourceNegativeTestCaseCreator.InsertLeadSourceValidationTest[input];

            _expected = (string)_request[1];
            LM_Master_SourceInputEntities _leadMgmtSourceInputEntities = new LM_Master_SourceInputEntities();
            _leadMgmtSourceInputEntities = (LM_Master_SourceInputEntities)_request[0];

            using (var mock = AutoMock.GetLoose())
            {
                mock.Mock<ILeadMgmtDAO>().Setup(x => x.InsertLeadSource(It.IsAny<LM_Master_SourceInputEntities>())).Returns(_success);
                var _leadmgmtBO = mock.Create<LeadMgmtBO>();
                var _actual = Assert.Throws<LeadManagementValidationException>(() => _leadmgmtBO.InsertLeadSource(_leadMgmtSourceInputEntities));

                Assert.Equal(_expected, _actual.Message);
            }
        }

        #endregion

        #endregion

        #region Private methods

        private List<LM_Master_SourceEntities> GetLeadMgmtSourceList()
        {
            List<LM_Master_SourceEntities> _leadManagementSourceCollection = new List<LM_Master_SourceEntities>();

            LM_Master_SourceEntities leadMgmtSourceEntities1 = new LM_Master_SourceEntities
            {
                LeadSourceId = 1,
                Sourcename = "ATM"
            };
            _leadManagementSourceCollection.Add(leadMgmtSourceEntities1);
            LM_Master_SourceEntities leadMgmtSourceEntities2 = new LM_Master_SourceEntities
            {
                LeadSourceId = 2,
                Sourcename = "Internet"
            };
            _leadManagementSourceCollection.Add(leadMgmtSourceEntities2);

            return _leadManagementSourceCollection;
        }

        #endregion

    }
}
