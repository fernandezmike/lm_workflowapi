﻿using LeadManagementEntities.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeadManagement.BO.UnitTest.TestCaseCreator
{
    public class InsertLeadSourceNegativeTestCaseCreator
    {
        #region Private Variables
        private static LM_Master_SourceInputEntities _leadMgmtSourceInputEntities_NullRequest = null;

        private static LM_Master_SourceInputEntities _leadMgmtSourceInputEntities_WithoutUserId = new LM_Master_SourceInputEntities
        {
            Sourcename = "Test"
        };
        private static LM_Master_SourceInputEntities _leadMgmtSourceInputEntities_WithoutSourceName = new LM_Master_SourceInputEntities
        {
            UserId = 1
        };

        #endregion

        #region Public Methods

        public static readonly List<object[]> InsertLeadSourceValidationTest = new List<object[]> {
            new object[] { _leadMgmtSourceInputEntities_NullRequest , "Required Input Parameters Not Passed" },
            new object[] { _leadMgmtSourceInputEntities_WithoutSourceName, "Source name is Required" },
            new object[] { _leadMgmtSourceInputEntities_WithoutUserId, "User Id 0 is not valid user" }
        };

        public static IEnumerable<object[]> InsertLeadSourceValidationIndex
        {
            get
            {
                List<object[]> _leadInsertValidationTest = new List<object[]>();
                for(int i = 0; i < InsertLeadSourceValidationTest.Count; i++)
                {
                    _leadInsertValidationTest.Add(new object[] { i });
                }
                return _leadInsertValidationTest;
            }
        }

        #endregion
    }
}
